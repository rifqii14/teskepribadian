    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="./../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="./../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="./../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="./../vendor/raphael/raphael.min.js"></script>
    <script src="./../vendor/morrisjs/morris.min.js"></script>
    <script src="./../data/morris-data.js"></script>

    <!-- DataTables JavaScript -->
    <script src="./../vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="./../vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="./../vendor/datatables-responsive/dataTables.responsive.js"></script>


    <script src="./../js/jspdf.js"></script>
    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
    <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/jszip.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="./../dist/js/sb-admin-2.js"></script>

</body>

</html>