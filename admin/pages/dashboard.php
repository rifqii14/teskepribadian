<?php require 'base/base_head.php'?>

<?php require 'base/base_navbar.php'?>



        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Jumlah Data Yang Sudah Masuk</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <?php   
                                        $query = 'SELECT a.id, a.nama, a.gugus,a.no_hp,a.alamat,a.mel,a.kol,a.pleg,a.sang, GROUP_CONCAT(b.jenis) FROM tbl_maba AS a INNER JOIN tbl_mabadetail AS b ON b.id_maba = a.id GROUP BY a.id ORDER BY gugus ASC';
                                        
                                        if($res = mysqli_query($conn,$query)){
                                            $count = mysqli_num_rows($res);
                                    
                                            echo '<div class="huge">'.$count.'</div>';
                                        }

                                    ?>
                                    <div><b>Jumlah Peserta</b></div>
                                </div>
                            </div>
                        </div>
                        <a href="./datamaba.php">
                            <div class="panel-footer">
                                <span class="pull-left">Lihat Data</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <?php

                for($i=1;$i<=20;$i++){

                    $query1 = 'SELECT a.id, a.nama, a.gugus,a.no_hp,a.alamat,a.mel,a.kol,a.pleg,a.sang, GROUP_CONCAT(b.jenis) FROM tbl_maba AS a INNER JOIN tbl_mabadetail AS b ON b.id_maba = a.id WHERE 
                        gugus = "'.$i.'" GROUP BY a.id ORDER BY a.gugus ASC  ';
                
                    if($i % 2 == 0){
                        echo '<div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-group fa-4x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">';

                                        if($row = mysqli_query($conn,$query1)){
                                            $counts = mysqli_num_rows($row);    
                                            echo 
                                            "<div class='huge'>".$counts."</div>
                                            <div><b>Peserta Gugus ".$i."</b></div>";
                                        }

                                    echo '</div>
                                    </div>
                                </div>
                                <a href="datapergugus.php?gugus='.$i.'">
                                    <div class="panel-footer">
                                        <span class="pull-left">Lihat Data</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>';
                    }else{
                        echo '<div class="col-lg-3 col-md-6">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-group fa-4x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">';

                                        if($row = mysqli_query($conn,$query1)){
                                            $counts = mysqli_num_rows($row);    
                                            echo 
                                            "<div class='huge'>".$counts."</div>
                                            <div><b>Peserta Gugus ".$i."</b></div>";
                                        }

                                    echo '</div>
                                    </div>
                                </div>
                                <a href="datapergugus.php?gugus='.$i.'">
                                    <div class="panel-footer">
                                        <span class="pull-left">Lihat Data</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>';
                    }
                } 
             ?>
            </div>
        </div>
        <!-- /#page-wrapper -->


<?php require 'base/base_foot.php'?>