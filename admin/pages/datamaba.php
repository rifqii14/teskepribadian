<?php require 'base/base_head.php'?>
<?php require 'base/base_navbar.php'?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Mahasiswa Baru</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Mahasiswa Baru
                            <div class="pull-right">
                                <button class="btn btn-success btn-xs" id="simpan" onclick="location.href='laporan/CetakMaba.php'">Simpan Sebagai PDF</button>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="maba">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Prodi</th>
                                        <th>HP</th>
                                        <th>Alamat</th>
                                        <th>Gugus</th>
                                        <th>Kepribadian</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $i = 1;
                                    $query = 'SELECT a.id, a.nama, a.gugus, a.prodi,a.no_hp,a.alamat,a.mel,a.kol,a.pleg,a.sang, GROUP_CONCAT(b.jenis) FROM tbl_maba AS a INNER JOIN tbl_mabadetail AS b ON b.id_maba = a.id GROUP BY a.id ORDER BY gugus ASC';
                                    $res = mysqli_query($conn,$query);
                                      while($row = mysqli_fetch_assoc($res)){
                                        echo                
                                        "<tr>
                                            <td style='width:5%'><center>".$i++."</center></td>
                                            <td>".$row['nama']."</td>
                                            <td>".$row['prodi']."</td>
                                            <td>".$row['no_hp']."</td>
                                            <td>".$row['alamat']."</td>
                                            <td>Gugus ".$row['gugus']."</td>
                                            <td>".$row['GROUP_CONCAT(b.jenis)']."</td>
                                            <td><center><a href='datamabadetail.php?id=".$row['id']."'>Detail</a></center></td>
                                        </tr>";
                                      }
                                ?>
                               </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->


<?php require 'base/base_foot.php'?>

<script>
$(document).ready(function() {
    $('#maba').DataTable({
        responsive: true,
        lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
    });
});

</script>