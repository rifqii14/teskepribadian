<?php require 'base/base_head.php'?>
<?php require 'base/base_navbar.php'?>
<?php  $id = $_GET['id'];  ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Data Mahasiswa Baru</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Detail
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
   
                            <?php     

                            function nilaimax($pa,$pi,$pu,$pe){
                                $max=$pa;
                                if($max<$pi) $max = $pi;
                                if($max<$pu) $max = $pu;
                                if($max<$pe) $max = $pe;

                                return $max;
                            }

                            $query = 'SELECT a.id, a.nama, a.prodi, a.gugus,a.no_hp,a.alamat,a.mel,a.kol,a.pleg,a.sang, GROUP_CONCAT(b.jenis) FROM tbl_maba AS a INNER JOIN tbl_mabadetail AS b ON b.id_maba = a.id WHERE 
                                        a.id = "'.$id.'" GROUP BY a.id ORDER BY gugus ASC';

                            $res = mysqli_query($conn,$query);
                            $row = mysqli_fetch_assoc($res);

                            $melankolis = $row['mel'];
                            $koleris = $row['kol'];
                            $plegmatis = $row['pleg'];
                            $sanguinis = $row['sang'];

                            $max=nilaimax($melankolis,$sanguinis,$koleris,$plegmatis);

                            echo "
                            <table width='100%'' class='table table-striped table-bordered table-hover' id='maba'>
                                <tbody
                                    <tr>
                                        <td><b>Nama</b> : ".$row['nama']." </td>";

                                   if($max==$koleris){    
                                        echo "<td rowspan=2> Nilai Koleris : ".$row['kol']."</td>";
                                        $jenis1=4;
                                   }else{
                                        echo "<td rowspan=2> Nilai Koleris : ".$row['kol']."</td>";
                                   }
                                   echo "
                                    <tr>
                                        <td> <b> Prodi</b> : ".$row['prodi']."</td>
                                    </tr>
                                   ";
                                   echo "</tr>
                                    <tr>
                                        <td><b>No Hp</b> : ".$row['no_hp']." </td>";

                                    if($max==$melankolis){    
                                            echo "<td> Nilai Melankolis     : ".$row['mel']."</td>";
                                            $jenis2=2;
                                    }else{
                                            echo "<td> Nilai Melankolis     : ".$row['mel']."</td>";
                                    }

                                    echo "</tr>
                                     <tr>
                                         <td><b>Alamat</b> : ".$row['alamat']." </td>";

                                     if($max==$sanguinis){    
                                             echo "<td> Nilai Sanguinis     : ".$row['sang']."</td>";
                                             $jenis3=1;
                                     }else{
                                             echo "<td> Nilai Sanguinis     : ".$row['sang']."</td>";
                                     }

                                     echo "</tr>
                                      <tr>
                                          <td><b>Gugus</b> : ".$row['gugus']." </td>";

                                      if($max==$plegmatis){    
                                              echo "<td> Nilai Plegmatis    : ".$row['pleg']."</td>";
                                              $jenis4=3;
                                      }else{
                                              echo "<td> Nilai Plegmatis    : ".$row['pleg']."</td>";
                                      }

                                    echo"
                                    <tr>
                                        <td align='center' colspan=2> Jenis Kepribadian yang Menonjol : 
                                    </tr>
                                </tbody>
                            </table>";
                            ?>

                            <?php 

                               if (!isset($jenis1)) $jenis1='';
                               if (!isset($jenis2)) $jenis2='';
                               if (!isset($jenis3)) $jenis3='';
                               if (!isset($jenis4)) $jenis4='';

                               $res = mysqli_query($conn,"SELECT * FROM tbl_hasil WHERE id='$jenis1' OR id='$jenis2' OR id='$jenis3' OR id='$jenis4'");

                               // echo $jenis3;
                            while($row=mysqli_fetch_assoc($res)){
                            echo '<div class="panel panel-red">
                                <div class="panel-heading">
                                <b>
                                    '.$row['jenis'].'
                                </b>
                                </div>
                                <!-- .panel-heading -->
                                <div class="panel-body">
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                <b>
                                                    Ciri-ciri
                                                </b>
                                                </h4>
                                            </div>
                                            
                                                <div class="panel-body">
                                                    '.$row['ciri'].'
                                                </div>
                                           
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                <b>
                                                    Kekuatan
                                                </b>
                                                </h4>
                                            </div>
                                            
                                                <div class="panel-body">
                                                
                                                    '.$row['kekuatan'].'.
                                                </div>
                                            
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                <b>
                                                    Kelemahan
                                                </b>
                                                </h4>
                                            </div>
                                            
                                                <div class="panel-body">
                                                    '.$row['kelemahan'].'
                                                </div>
                                            
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    Pekerjaan yang cocok
                                                </h4>
                                            </div>
                                            
                                                <div class="panel-body">
                                                    '.$row['cocok'].'
                                                </div>
                                           
                                        </div>
                                    </div>
                                </div>
                                <!-- .panel-body -->
                            </div>';
                        }?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
        <!-- /#page-wrapper -->


<?php require 'base/base_foot.php'?>