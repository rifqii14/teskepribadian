-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 28, 2018 at 02:03 PM
-- Server version: 5.5.60-cll
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `k6986450_kepribadian`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hasil`
--

CREATE TABLE `tbl_hasil` (
  `id` int(11) NOT NULL,
  `jenis` enum('Sanguinis','Melankolis','Plegmatis','Koleris') NOT NULL,
  `ciri` text NOT NULL,
  `kekuatan` text NOT NULL,
  `kelemahan` text NOT NULL,
  `cocok` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_hasil`
--

INSERT INTO `tbl_hasil` (`id`, `jenis`, `ciri`, `kekuatan`, `kelemahan`, `cocok`) VALUES
(1, 'Sanguinis', 'Cenderung populer dan disenangi banyak orang, Hidupnya penuh dengan bunga warna-warni, Senang berbicara tanpa dihentikan, Gejolak emosinya bergelombang dan transparan, Senang menolong orang lain tetapi tidak dapat jadi sandaran, Pada suatu saat dia teriak kegirangan kemudian beberapa saat dia bisa jadi menangis tersedu-sedu, Sedikit pelupa, sulit berkonsentrasi, cenderung berfikir pendek, dan hidupnya serba tidak teratur.', 'Senang berbicara, Secara fisik memegang pendengar, demonstrative, dan emosional; Ceria, Penuh rasa ingin tahu, Mudah berubah (banyaknya kegiatan / keinginan), Berhati tulus dan kekanak-kanakan, Mudah berteman dan menyukai orang lain, Senang berkumpul (untuk bertemu dan berbicara), Mudah memaafkan dan tidak menyimpan dendam.', 'Membesar-besarkan suatu hal / kejadian, Susah untuk diam, Suara tertawa yang terlalu keras, Mudah ikut-ikutan atau dikendalikan oleh keadaan atau orang lain, Rentan konsentrasi pendek, Sering minta persetujuan termasuk hal-hal sepele, Mudah marah, Mudah berubah-ubah, Egois, Prioritas kegiatan kacau, Konsentrasi ke <b>How to spend money</b> daripada <b>How to earn / save money</b>', 'Artis, MC, Pengacara.'),
(2, 'Melankolis', 'Umumnya mereka ini suka dengan fakta-fakta, data-data, angka-angka dan sering sekali memikirkan segalanya secara mendalam . Dalam sebuah pertemuan, orang sanguinis selalu saja mendominasi pembicaraan, namun orang melankolis cenderung menganalisa, memikirkan, mempertimbangkan, lalu kalau bicara pastilah apa yang ia katakan betul-betul hasil yang ia pikirkan secara  mendalam sekali. <br> Orang melankolis selalu ingin serba sempurna, Segala sesuatu ingin teratur.', 'Analitis, mendalam, dan penuh pikiran; Serius dan bertujuan, serta berorientasi jadwal; Artistik, musikal dan kreatif (filsafat & puitis), Sensitif, Mau mengorbankan diri dan idealis, Standar tinggi dan perfeksionis, Hemat, Berteman dengan hati-hati, Sangat memperhatikan orang lain, Menghindari Perhatian, kreatif (sering terlalu kreatif), Melihat masalah dan mencari solusi pemecahan.', 'Cenderung melihat masalah dari sisi negatif (murung dan tertekan), Mengingat yang negatif & pendendam, Mudah merasa bersalah dan memiliki citra diri rendah, Lebih menekankan pada cara daripada tercapainya tujuan, Sulit bersosialisasi, Tertekan pada situasi yg tidak sempurna dan berubah-ubah, Melewatkan banyak waktu untuk menganalisa dan merencanakan, Sulit mengungkapkan perasaan (cenderung menahan kasih sayang), Rasa curiga yg besar (skeptis terhadap pujian), Standar yang terlalu tinggi sehingga sulit disenangi.', 'Seniman, Musisi, Penulis, dsb.'),
(3, 'Plegmatis', 'Ia mau merugi sedikit atau rela sakit, asalkan masalahnya nggak terus berkepanjangan; Kaum phlegmatis kurang bersemangat, kurang teratur dan serba dingin; Cenderung diam, kalem dan kalau memecahkan masalah umumnya sangat menyenangkan; Dengan sabar ia mau jadi pendengar yang baik, tapi kalau disuruh untuk mengambil keputusan ia akan terus menunda-nunda, Kadang sedikit serba salah berurusan dengan para plegmatis ini. Ibarat keledai, <b>kalau didorong ngambek, tapi kalau dibiarin nggak jalan</b>.', 'Mudah bergaul, santai, tenang dan teguh; Sabar, seimbang, dan pendengar yang baik; Tidak banyak bicara, tetapi cenderung bijaksana; Simpatik dan baik hati (sering menyembunyikan emosi), Kuat di bidang administrasi, dan cenderung ingin segalanya terorganisasi; Penengah masalah yg baik, Cenderung berusaha menemukan cara termudah, Baik dibawah tekanan, Menyenangkan dan tidak suka menyinggung perasaan, Rasa humor yg tajam, Senang melihat dan mengawasi, Berbelaskasihan dan peduli, Mudah diajak rukun dan damai.', 'Kurang antusias, terutama terhadap perubahan/ kegiatan baru; Takut dan khawatir, Menghindari konflik dan tanggung jawab; Keras kepala, sulit kompromi (karena merasa benar), Terlalu pemalu dan pendiam, Humor kering dan mengejek (Sarkatis), Kurang berorientasi pada tujuan, Sulit bergerak dan kurang memotivasi diri, Lebih suka sebagai penonton daripada terlibat, Tidak senang didesak-desak, Menunda-nunda / menggantungkan masalah.', 'Guru, Akuntan, dan lain sebagainya.'),
(4, 'Koleris', 'Suka sekali mengatur orang, Suka menunjuk atau memerintah orang, Senang dengan tantangan, suka petualangan; Orang-orang berusaha menghindar dan menjauh agar tak jadi korban karakternya yang suka mengatur dan tak mau kalah itu, Tegas, kuat, cepat dan tangkas dalam mengerjakan. Ia tak ingin ada penonton dalam aktivitasnya, bahkan tamu pun bisa saja ia suruh melakukan sesuatu untuknya. Akibat sifatnya yang <b>Bossy</b> itu membuat banyak orang koleris tak punya banyak teman.', 'Senang memimpin, membuat keputusan, dinamis dan aktif (Seorang Leader), Sangat memerlukan perubahan dan harus mengoreksi kesalahan, Berkemauan keras dan pasti untuk mencapai sasaran/ target, Bebas dan mandiri, Berani menghadapi tantangan dan masalah, Mencari pemecahan praktis dan bergerak cepat, Mendelegasikan pekerjaan dan orientasi berfokus pada produktivitas, Membuat dan menentukan tujuan, Terdorong oleh tantangan dan tantangan, Tidak begitu perlu teman, Biasanya benar dan punya visi ke depan.', 'Tidak sabar dan cepat marah, Senang memerintah, Terlalu bergairah dan susah untuk santai, Menyukai kontroversi dan pertengkaran, Terlalu kaku dan kuat/ keras, Tidak suka yang bertele-tele / terlalu rinci, Sering membuat keputusan tergesa-gesa ,Memanipulasi dan menuntut orang lain, cenderung memperalat orang lain;, Menghalalkan segala cara demi tercapainya tujuan, Mungkin selalu benar tetapi tidak popular, Tidak menyukai air mata dan emosi.', 'Biasanya berhubungan dengan kepemimpinan karena kaum ini mempunyai jiwa kepemimpinan yang sangat tinggi.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_maba`
--

CREATE TABLE `tbl_maba` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `gugus` int(11) NOT NULL,
  `no_hp` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `prodi` enum('Informatika','Elektro','Mesin','') NOT NULL,
  `mel` int(11) DEFAULT NULL,
  `kol` int(11) DEFAULT NULL,
  `pleg` int(11) DEFAULT NULL,
  `sang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_maba`
--

INSERT INTO `tbl_maba` (`id`, `nama`, `gugus`, `no_hp`, `alamat`, `prodi`, `mel`, `kol`, `pleg`, `sang`) VALUES
(78, 'Abdul fatah ', 18, '085217770659', 'Kp parung tanjung', 'Informatika', 7, 7, 4, 2),
(79, 'Tiara Maulida Sedena', 18, '085881957467', 'Jl Rawajati Barat II', 'Informatika', 2, 2, 3, 13),
(80, 'Arief Nurhidayatullah', 18, '085603828810', 'desa ubrug rt 01 rw 10 kec. warung kiara kab. sukabumi jawa barat', 'Mesin', 3, 6, 6, 5),
(81, 'Muhamad rizki pratama', 18, '087888050419', 'Jalan karang tengah taman sari 2 Rat001/03 no.66 kec.cilandak kel.lebak bulus 12440', 'Informatika', 5, 4, 9, 2),
(82, 'Fandy Rizki Ramadhan', 18, '081318405988', 'jl.perintis kemerdekaan Pulo gadung Jakarta timur', 'Informatika', 10, 2, 7, 1),
(83, 'Afi Khoiril Anam', 18, '081385147968', 'Puri Bojong Lestari 1 Blok AO/9 Pabuaran Bojonggede', 'Informatika', 5, 3, 5, 7),
(84, 'Yusca Arie Maulana', 18, '085772897336', 'Perum rancamanyar kec kota baru kabupaten karawang', 'Mesin', 5, 8, 2, 5),
(85, 'Reza aditya', 18, '089635567287', 'Jl anggrek merah 1 No 29 kaliabang tengah bekasi utara', 'Elektro', 4, 6, 5, 5),
(86, 'Radi Syahputra', 18, '087882292018', 'Jl. Raya bogor kp.kramat', 'Mesin', 4, 6, 6, 4),
(87, 'R Mohammad Alansyah Mauludi', 18, '081973319009', 'jl masjid nurul al falah no 36 kel pondok ranggon kec cipayung', 'Mesin', 7, 1, 6, 6),
(89, 'Febrian Tri Handayani', 18, '081292428613', 'Ciracas', 'Informatika', 1, 3, 10, 6),
(90, 'Nur Amilah', 18, '08159887509', 'Kp. Pagedangan, Ds. Ranca Bango, Kec. Rajeg, Kab. Tangeran,  Prop. Banten', 'Informatika', 8, 4, 1, 7),
(91, 'Daffa Firman Alfarisyi', 18, '083817116787', 'Jl. Asgo 2 RT 016 RW 03 Kelurahan Rambutan, Kecamatan Ciracas, Provinsi DKI Jakarta', 'Informatika', 5, 5, 7, 3),
(92, 'Ihsan hibatur rahman', 17, '082246717707', 'Jl.wahid khasim,limo,depok', 'Informatika', 6, 4, 5, 5),
(93, 'Hafizh Fakhri', 17, '085814293335', 'Jl.Rambutan no.23 RT/RW 02/03', 'Informatika', 3, 1, 15, 1),
(94, 'Juliyansyah Dwi Putra ', 17, '082114114281', 'Jl Lebak Para 1 rt004/05 no 27 cijantung pasar Rebo Jakarta timur', 'Informatika', 3, 2, 9, 6),
(95, 'Ari Setianto', 17, '089651733186', 'Jl h. Bara no 52 pangkalan jati baru cinere depok', 'Mesin', 5, 2, 9, 4),
(96, 'Puguh kujatmiko', 17, '087788145823', 'Jatijajar rt 004/006 no.57 Kel.Jatijajar Kec.Tapos Depok', 'Elektro', 6, 2, 9, 3),
(97, 'Dzikri prayoga azhari', 17, '081284649581', 'Taman tridaya indah c.10 no.9 , kab. Bekasi', 'Mesin', 5, 7, 3, 5),
(98, 'Muhammad ampel eka purnama', 17, '081211824052', 'Jl. Penggilingan baru 3 rt 16/4 no36 kel.dukuh kec.kramat jati', 'Mesin', 4, 3, 4, 9),
(99, 'Atiqah fauziah', 17, '0838-1555-8665', 'Kp.selang nangka kec.cibitung kab.Bekasi', 'Elektro', 3, 2, 9, 6),
(100, 'Robi Firdaus ', 17, '089651511357 ', 'Kp.pasauran kec.cinangka kab.serang provinsi.banten', 'Informatika', 4, 2, 10, 4),
(101, 'Muhammad Alip Prasetyo', 17, '081292829224', 'Jl moncokerto raya 3 no 18', 'Informatika', 6, 7, 4, 3),
(102, 'Echaparhamda', 17, '089508846709', 'jl.h.muin rt 09 rw 09', 'Informatika', 8, 6, 4, 2),
(103, 'Muhammad Daffa Ramdaniel', 5, '0813-8634-7563', 'Jl.Perdana/102 Rt 005/003 kel Ulujami kec Pesanggrahan Jakarta Selatan', 'Informatika', 8, 2, 9, 1),
(104, 'Thoriq amirulloh', 17, '089653181717', 'Citra indah bukit cemara p20/10 jonggol , kab. Bogor', 'Mesin', 5, 2, 7, 6),
(105, 'Ikhsan Juwanda Nur Fauzan', 18, '0895389450478', 'villa mutiara cikarang blok g.1/no.08', 'Mesin', 0, 10, 2, 8),
(106, 'Fasya Nazihah', 4, '089699281150', 'Metland cileungsi cluster melati', 'Informatika', 7, 3, 6, 4),
(107, 'Idzhar Noor Ardiansyah Nasution', 17, '08816180101', 'Perumahan Taman Cikarang Indah Blok B6 No30', 'Mesin', 4, 6, 6, 4),
(108, 'Abdul Salam Zulkarnain', 17, '0895332477245', 'Mahkota indah blok HA XI NO16A,mangun jaya tambun selatan', 'Informatika', 4, 9, 4, 3),
(109, 'Dhiva Mahendra Arifin', 5, '089618056083', 'jl Cipayung pondok Rajeg RT 002 RW 05 Cibinong Bogor', 'Elektro', 3, 5, 7, 5),
(110, 'Nusaibah fatihah izzati', 5, '085775268011', 'Jl. Cililitan besar rt.09/01 no.07', 'Informatika', 5, 8, 5, 2),
(111, 'Ahmad farhan akrom', 17, '081381139857', 'Jln. Agung raya 1 lenteng agung jaksel', 'Informatika', 9, 1, 6, 4),
(112, 'ibnuhamzah', 17, '085894969446', 'jln.kesadaran rt08/rw01 cipinang muara kec jatinrgara jakarta timur', 'Informatika', 5, 3, 6, 6),
(113, 'ranti alfira natasya', 15, '081386117340', 'jln.leuwinanggung kec tapos kot depok', 'Informatika', 5, 4, 5, 6),
(114, 'Hafifsyah Rifaldi', 17, '081317011037', 'Jl. Damar 5 RT06/RW 06 Pekayon Jaya 2, Bekasi Selatan', 'Informatika', 9, 3, 7, 1),
(115, 'JIHAN FARERA', 4, '085959242378', 'Perumahan Cileungsi Permai Blok D.12 / RT 2 / RW 12', 'Informatika', 7, 1, 7, 5),
(116, 'Muhammad Zaidan', 4, '0895610510514', 'Jl. Swadaya no.108 rt 11/01 kel. Tengah kec. Kramat jati Jakarta timur', 'Informatika', 2, 0, 15, 6),
(117, 'Farhan Firjatullah ', 4, '087877655515', 'jln gg hj musanif Rt 001 Rw 08', 'Informatika', 2, 2, 6, 10),
(118, 'Kevin aprilianda', 5, '087885098055 ', 'Jl.h.naseh Rt.006/04. Lenteng agung. Jakarta selatan', 'Elektro', 7, 2, 8, 3),
(119, 'Karennina Amalia Putri', 12, '089601678720', 'Jl. Tipar cakung, gang damai rt/rw 016/07, cakung, jakarta timur', 'Informatika', 7, 1, 5, 7),
(120, 'Handika Ramadhan', 4, '089630980132', 'Jl.Batu Ampar Kec.Kramat Jati Rt16/Rw02 Jakarta Timur', 'Informatika', 8, 0, 9, 3),
(121, 'Revina Damawati ', 5, '085714673665', 'Komplek perumahan Departemen kesehatan block c3 no6 ciputat', 'Informatika', 4, 6, 4, 6),
(122, 'Nur Hanafi', 4, '0895331779928', 'Jl.suci rt 10 rw 03 susukan Ciracas Jakarta timur', 'Elektro', 9, 1, 10, 0),
(123, 'Ivan Nugraha ', 5, '08381221335', 'Jl. Delima no 10', 'Mesin', 7, 5, 6, 2),
(124, 'Eky Dwi Cahyadi', 5, '087877459886', 'Jalan Ali Cipayung Jakarta Timur', 'Informatika', 6, 4, 7, 3),
(125, 'Emas fyqri nurdwiprasetio hasan', 12, '087886637025', 'Rangkasbitung kab lebak prov banten', 'Elektro', 10, 7, 1, 2),
(126, 'DICKY RUDIANSYAH', 5, '081312677461', 'Jl. Gunuk V rt.09 rw. 03 poltangan, pasar minggu, jakarta selatan', 'Elektro', 2, 4, 8, 6),
(127, 'Muhammad Adam Fauzan', 13, '087898744833', 'KOPASSUS SERANG BANTEN', 'Informatika', 9, 3, 6, 2),
(128, 'Yoga Aditya Setiawan', 12, '085884230786', 'Perumahan Metland cileungsi blok AA 17/15 rt 001/011 Kec.cileungsi kab.bogor', 'Mesin', 5, 3, 9, 3),
(129, 'Aji firmansyah', 12, '087714335565', 'Pasarkemis tangerang', 'Informatika', 5, 6, 6, 3),
(130, 'Akhmad muzadi', 13, '082324643553', 'Perum asabri jatiluhur, jatiasih bekasi', 'Informatika', 7, 7, 3, 3),
(131, 'Feriawan Ikhsan W', 6, '085886983115', 'Cilodong', 'Informatika', 7, 2, 3, 8),
(132, 'Annisa Shifah Fauziah Faturohman', 12, '081319957646', 'Perumahan Cibarusah Indah Blok B12A No 16 Rt 003 Rw 007 Cibarusah,Bekasi,Jawa Barat', 'Informatika', 9, 2, 7, 2),
(133, 'Hardian Ferdy Hidayat', 13, '082112693391', 'Jalan Dahlia VII Bekasi Tambun Selatan', 'Informatika', 3, 6, 7, 4),
(134, 'FARAH HIDAYATUL AFIFAH', 13, '0895389737173', 'Kp. Sidamukti Rt.03/02 no.25 Cilodong, Depok', 'Informatika', 9, 2, 2, 7),
(135, 'Raditya Hafidz Pradana Putra ', 13, '081932979486', 'Jl.madrasah no 37', 'Informatika', 5, 4, 7, 4),
(136, 'Hafizh Raihan Murniawan', 12, '08568952690', 'Kp.sasak bakar rt 01/ rw 01 ds.kertamukti kec.cibitung kab.bekasi', 'Mesin', 7, 5, 4, 4),
(137, 'Wahyu pratama', 12, '082260107142', 'Gg. Ikhlas 4, kebagusan, pasar minggu, jaksel', 'Informatika', 3, 8, 5, 4),
(138, 'Mochamad Rizki Febryan', 17, '089607361766', 'Kp.tegal Rt 24/07 Desa Kembangkuning Kec. Klapanunggal - Bogor', 'Informatika', 7, 7, 4, 2),
(139, 'Bayufauzan', 5, '089512866213', 'Jl.madrasah Rt.08 Rw.05 Susukan Ciracas Jakarta Timur', 'Mesin', 11, 1, 6, 2),
(140, 'Wiar Winengsih ', 13, '089675253374', 'Jln. Damai kp. Kalimanggis Bekasi', 'Informatika', 7, 3, 7, 3),
(141, 'Ari Purnomo Aji', 20, '081280886376', 'JL.BENTENGAN RT02 RW01 NO.14D', 'Informatika', 4, 2, 11, 3),
(142, 'Riky Ardiansyah ', 5, '083819035315 ', 'Jl. Gurusuma RT. 03/08 No. 106 Kec. Cibinong Kab. Bogor', 'Informatika', 3, 3, 5, 9),
(143, 'Rafli Aditya Rakasiwi', 15, '085884817519', 'Perum telaga murni Blok C11 no 4', 'Informatika', 4, 4, 5, 7),
(144, 'Khairum majid wibowo', 6, '089504680573', 'Kampung makasar jakarta timur', 'Elektro', 9, 4, 5, 2),
(145, 'Faris Setio wibowo', 2, '085743968137', 'Cijantung', 'Mesin', 5, 4, 7, 4),
(146, 'Aldony Thoriq', 6, '0895330075004', 'Jl.Taruna Jaya rt;04/13', 'Mesin', 4, 7, 6, 3),
(147, 'Arya Fara Firmansyah', 6, '081271235605', 'Jalan transad 5 no 23', 'Informatika', 4, 5, 3, 8),
(148, 'Dicky julian ', 13, '087842304753', 'Gg kinoy cibubur jakarta timur', 'Informatika', 7, 3, 5, 5),
(149, 'Dimas Enggal Mukti', 4, '081315930054', 'Jl. Kopral Daman rt/rw 01/03 sawangan baru no. 6 kota Depok', 'Informatika', 6, 6, 3, 5),
(150, 'restu heryono', 15, '085891984240', 'kp.sawah rt 12/05 kec.mauk kab.tangerang banten', 'Informatika', 5, 3, 0, 12),
(151, 'Mahardhika Dava Wardhana', 20, '08558033263', 'Wahana pondok gede Blok L2 no 35', 'Elektro', 7, 2, 6, 5),
(152, 'ZULFA FAADILLAH FITRIANI', 13, '085959312675', 'Cileungsi, Bogor.', 'Elektro', 3, 8, 4, 5),
(153, 'Fahmi Triyadi', 2, '089612847172', 'Jalan Pringgodani no 69A pondok labu Jakarta Selatan Cilandak 12450', 'Informatika', 3, 1, 9, 7),
(155, 'Surya Andika Saputra', 20, '081289710620', 'Jl.F Gg.K RT.002/02 No.2 KEL.RAWA BADAK UTARA KEC.KOJA', 'Mesin', 8, 6, 2, 4),
(156, 'FADHILAH ISMAIL', 12, '087738767542', 'Cibitung, Bekasi', 'Informatika', 8, 2, 6, 4),
(157, 'Hafizh Faqihuddin', 6, '089647511731', 'Condet', 'Informatika', 5, 5, 5, 5),
(158, 'AXCEL AJENCI RAPLES', 13, '08886127634', 'Jln tipar cakung KP BARU.', 'Mesin', 2, 5, 5, 8),
(159, 'Satria Eka Rifaldi', 13, '087879541145', 'Perum KSB Blok E19 no.22 Serang Baru Kab. Bekasi', 'Informatika', 9, 1, 4, 6),
(160, 'Arya Wira Nugraha', 12, '089523307511', 'Jl . Terkira 1 no.103, rt 03 rw 09 kel. Gedong, kec. Pasar rebo, jakarta timur', 'Mesin', 3, 5, 9, 3),
(161, 'Anas Mabruki', 13, '08978664207', 'Jl.guru mughni no 10 kuningan timur jaksel', 'Informatika', 3, 4, 4, 9),
(162, 'Adrakha mathorig', 13, '081398656921', 'Twp tni al ciangsana', 'Informatika', 8, 2, 4, 6),
(163, 'Faturrohman', 15, '08558306689', 'Ko risah RT 08/03 DS Kedung dalem kec Mauk kab Tangerang banten', 'Informatika', 7, 3, 4, 6),
(164, 'Fadhika Julian Nugroho', 12, '081219789870', 'Kabupaten Bekasi, kecamatan Tarumajaya', 'Elektro', 7, 3, 5, 5),
(165, 'Hisyam Septio Muhammad', 12, '085779392591', 'Parung Serab,Ciledug Tangerang', 'Informatika', 8, 3, 6, 3),
(166, 'Fajar Pangestu Amandaru', 12, '089661292501', 'Kecamatan Cibinong, Kabupaten Bogor', 'Informatika', 0, 7, 5, 8),
(167, 'ikhtiar mulya laksana', 15, '08811871451', 'jl. saorma kalisari rt 04 rw 1', 'Informatika', 4, 6, 3, 7),
(168, 'Setiawan Sareng', 4, '0895610399920', 'Jl.tipar cakung Gg.kompi jenggot rt07/01 kelurahan sukapura kecamatan cilincing , Jakarta Utara 14140', 'Informatika', 4, 1, 3, 12),
(169, 'Widia Putri Septiani', 20, '081316815940', 'Kp.Pasar Inun, rt/rw 001/005, kec cipeucang, pandeglang Banten', 'Informatika', 5, 3, 4, 8),
(170, 'Shita Artamonica', 6, '081317467420', 'Tambun selatan', 'Informatika', 4, 5, 6, 5),
(171, 'YUSUF HANDIKA', 13, ' 6289676796609', 'Villa Mutiara Blok L 21/32 Cibitung, Bekasi', 'Informatika', 4, 4, 7, 5),
(172, 'Anugrah bagus perbawa', 20, '081288887513', 'jl.cipinang besar selatan rt01/10 no.26', 'Informatika', 5, 6, 3, 6),
(173, 'Octarian geger amanda', 6, '0895333538350', 'Bekasi utara', 'Informatika', 1, 3, 7, 9),
(174, 'Algivari rido ramadhan', 12, '081908696363', 'Jl.tenggiri 13 no.235 kec.kayuringin jaya kel.bekasi selatan kota bekasi', 'Mesin', 5, 3, 6, 6),
(175, 'Muhammad Rizky Prasetyo', 12, '089517754759', 'Kecamatan ciracas, jakarta timur', 'Informatika', 8, 2, 3, 7),
(176, 'RIZKI ALIF AMINUDIN', 20, '085880079902', 'Kp.babakan kelurahan sukatani kecamatan tapos rt03/10 no 95', 'Mesin', 7, 3, 4, 6),
(177, 'Nur Fadla Rizki', 6, '083877705629', 'Jakarta Selatan', 'Mesin', 8, 3, 8, 1),
(178, 'Prista afikah', 2, '081995662902', 'Jl. Angsana toboali', 'Informatika', 4, 4, 7, 5),
(179, 'Bangkit Cipta Setiawan', 2, '089607517725', 'Jl.bhakti abri rt.04/08 sukamaju baru Tapos DEPOK', 'Mesin', 5, 6, 2, 7),
(180, 'Mukhammad Alan Zainur', 12, '0895393044780', 'Cilengsi bogor', 'Informatika', 6, 5, 6, 3),
(181, 'Andika Prasetya', 13, '089676706043', 'Jl.jambu raya Blok s3 no 7 rt08/20 sukatani, Tapos, Depok', 'Informatika', 6, 4, 7, 3),
(182, 'Andika rizqy julianto', 6, '0895603030492', 'Cibinong', 'Informatika', 2, 4, 9, 5),
(183, 'Farid Abdul Azhim', 13, '082249387553', 'Kp.pisangan rt008/05 Penggilingan Cakung Jakarta Timur', 'Elektro', 2, 5, 12, 1),
(184, 'Hendra Kurniawan', 20, '081212683860', 'Jl.Candra No.649 RT06 RW09 Halim P. Makasar Jakarta Timur 13610', 'Informatika', 4, 8, 3, 5),
(185, 'Rayyan Lintang Buwono', 12, '082213520572', 'Bukit pabuaran indah Blok L1 24 cilodong, cibinong-bogor 16916', 'Elektro', 6, 9, 1, 4),
(186, 'Muhammad Syukron Firdaus', 6, '087779076086', 'jakarta pusat', 'Informatika', 8, 4, 7, 1),
(187, 'Muhammad Isya Ramdhani', 20, '081282467016', 'Perum Trias blok G1/26 RT003 RW014 Kel.Wanasari Kec.Cibitung - Bekasi', 'Mesin', 3, 6, 9, 2),
(188, 'Alghifari Arief Noerwangsa', 9, '085883871977', 'Jl. Madrasah No. 23 Rt 01/04, Petukangan Selatan, Jakarta Selatan', 'Informatika', 5, 5, 9, 1),
(189, 'Huan Brian Saputra', 15, '0895412751396', 'Jl Mandala 5 no 29 003/02 kelurahan Cililitan kecamatan Kramatjati', 'Informatika', 7, 5, 7, 1),
(190, 'Fariz Mahardika Pramuji', 20, '081944513917', 'Jl. Bhakti raya no.1 RT/RW 04/06', 'Informatika', 3, 3, 3, 11),
(191, 'Bagas prakoso', 6, '082235004141', 'Jl.lapan,gg.lewa17 rt006/10 no.35', 'Informatika', 2, 5, 6, 7),
(192, 'Muhammad Ikmalul ulum hairis', 2, '089515171915', 'Perum griya praja mandiri block D6/8', 'Mesin', 9, 3, 6, 2),
(193, 'Muhammad Ikhsan Fikri ', 20, '089611584260', 'Kp.Susukan Rt.05/02 No.48 Kec.Bojonggede Kab.Bogor', 'Informatika', 9, 6, 1, 3),
(194, 'Aulia nabilah', 16, '082213415437', 'Bekasi timur', 'Elektro', 5, 5, 3, 7),
(195, 'Arya Bima Fauzan', 15, '081319906169', 'JL. TIRTA KENCANA 1 NO. 19 RT 10/03 KEL. TIRTAJAYA KEC. SUKMAJAYA KOTA DEPOK Kota Depok   Jawa Barat 16412', 'Informatika', 3, 2, 2, 3),
(196, 'burhanuddin rasyid ', 2, '0895326394111 ', 'jalan gudang air rt 02 rw 02 no. 25 a', 'Informatika', 5, 2, 9, 4),
(197, 'EEF SYAIFULOH', 9, '087773694153', 'Jl Mampang Prapatan 6 GG h Mugni no76 rt02rw02', 'Mesin', 3, 6, 3, 8),
(198, 'Galih Agung Sukmawan', 8, '081287268906', 'Jl.Cipayung Barat 1 Rt 06 RW 02 No.5', 'Informatika', 6, 4, 5, 5),
(199, 'Firli subhi', 16, '089517787560', 'Kp.parung serab rt 05/02 kel.tirtajaya kec.sukmajaya no.67 depok', 'Informatika', 5, 3, 9, 3),
(200, 'Dimas rahmat ramadhan', 2, '081280566304', 'Bekasi utara kel.harapan jaya rt 02/11', 'Informatika', 3, 4, 5, 8),
(201, 'Dandi Purnomo', 2, '085887225464', 'Perum Griya Nusa Citra kec. Jatiasih kel. Jatisari Kota Bekasi', 'Informatika', 4, 5, 6, 5),
(202, 'Choirul prayoga', 2, '081211036369', 'Jl.penganten Ali', 'Mesin', 8, 4, 3, 5),
(203, 'Bayu Nugroho Saputro', 16, '081297626517', 'Taman Adiyasa blok p10 no 29 RT 02 RW 07 provinsi Banten kab.tangerang kec.solear desa cikuya', 'Informatika', 3, 5, 6, 6),
(204, 'Hamdhani Nurul Setiawan', 2, '083805524225', 'kp rawalintah desa mekar mukti kec. Cikarang Utara kab. Bekasi', 'Informatika', 1, 6, 3, 10),
(205, 'Reza Firdaus Setiaji', 16, '089601844975', 'Jln dongkal RT 04 RW 03 Sukatani Tapos depok', 'Mesin', 5, 3, 2, 10),
(206, 'IMAN NUR KHAQIM ', 5, '083861374764 ', 'Karang Jambu Rt 02 RW 04', 'Mesin', 7, 2, 5, 6),
(207, 'Muhamad Irfan Fadhillah', 2, '085711859308', 'Jalan Raya Ceger gg h.baneng rt006/003 no 1', 'Elektro', 4, 3, 6, 7),
(208, 'Farhan Ramadan', 20, '083873536101', 'Jalan kebon nanas selatan 3', 'Informatika', 3, 4, 4, 9),
(209, 'RIZKA NISA AQILA', 16, '089686989587', 'Jalan Baret Biru III Rt.06 Rw.03 No. 76 Kalisari. Pasar Rebo', 'Informatika', 11, 2, 3, 4),
(210, 'RAMADHAN NAUFAL TAUFIK ARRAGMAN', 4, '089650919744', 'Jalan abadi kec.curug kel.cimanggis kota depok jawa barat rt 04/05 no121', 'Mesin', 6, 3, 6, 5),
(211, 'Rafiqal tahara', 9, '0895412228069', 'Jalan prof soepomo rt.001/002 menteng dalam, tebet', 'Mesin', 4, 5, 1, 10),
(212, 'Abdul Aziiz', 2, '081293265896', 'Kp.pitara RT.02 RW.15 Pancoran mas depok', 'Informatika', 4, 5, 4, 7),
(213, 'Muhammad Nanda Baihaqi', 2, '085895963127', 'Jl Tanah Merdeka Jakarta timur', 'Informatika', 6, 2, 7, 5),
(214, 'Suhadi Dikian Santo', 6, '085782015440', 'Rawamangun, Jakarta Timur', 'Elektro', 6, 0, 9, 5),
(215, 'Luqman Abdur Rahman Malik', 16, '081776608811', 'Jalan PGT 4, Komplek Dwikora, Halim P.K, Jakarta', 'Informatika', 8, 4, 5, 3),
(216, 'WAHYU ADJI APRIANTO ', 8, '081292918553', 'perum griya asri taman mini JATIMAKMUR pondok gede', 'Informatika', 5, 3, 10, 2),
(217, 'Agung Budianto', 20, '0895600393763', 'Kp. Bahari gg.1 no.74 rt 005 rw 005 kel. Tg. Priok jakarta utara', 'Informatika', 4, 3, 7, 6),
(218, 'Muhammad Ghazil Haq Renuzza', 8, '087885348671', 'Jln.usman Harun komp.arafat rt001/005', 'Informatika', 5, 9, 1, 5),
(219, 'Muhammad Rizki Pratama', 8, '081220313293', 'Parung Bingung,Depok', 'Informatika', 4, 2, 10, 4),
(220, 'Salvara Hafsa Mubarika', 9, '085811848897', 'Jalan Garuda no 45 rt13/01 Cimanggis depok', 'Informatika', 6, 1, 11, 2),
(221, 'PAHMI', 4, '08526172215', 'Jl dewa ujung rt15/rw07 ciracas jakarta timur', 'Mesin', 3, 4, 9, 4),
(222, 'BIMA PRATAMA ', 2, '089639817525', 'Jln delima 5 jaktim', 'Mesin', 3, 7, 8, 2),
(223, 'Firman arif munandar', 9, '089660271663', 'Perum telaga murni blok e19 no 12', 'Mesin', 3, 8, 4, 5),
(224, 'Silvia anggraeni ', 16, '087783233184', 'Jln. Pengantin ali 1', 'Informatika', 6, 2, 4, 8),
(225, 'Panggih Ghifari Sujud ', 9, '085710800368 ', 'Jl.bantarjati no 36, rt 01/02, setu', 'Informatika', 4, 2, 11, 3),
(226, 'Lutfi Fauzi', 2, '082244934225', 'Cileungsi Kidul RT 2 RW 1 Gng. Masjid Almukaromah', 'Informatika', 4, 1, 8, 7),
(227, 'Rizky Himawansyah Putra ', 20, '081717805162', 'Taman Alamanda Blok A14 No 8 RT 004/011  Desa Karang Satria Kecamatan Tambun Utara Kabupaten Bekasi', 'Informatika', 4, 3, 3, 10),
(228, 'Daffa Iksani Pratama ', 19, '085887576425 ', 'Jl. Alamparung blok c7 no8 Ciseeng, Bogor', 'Informatika', 7, 3, 7, 3),
(229, 'Shandi Pratama Shaleh', 5, '083893942998', 'jl.dharma 6 no 10 rt11/rw04 , kel.baru, kec.pasar reborn, cijantung, Jakarta timur', 'Informatika', 4, 5, 8, 3),
(230, 'Gilang Fariz Akbar', 8, '082112117385', 'Jln.tekukur no.5 bukit duri', 'Informatika', 5, 2, 12, 1),
(231, 'Zidan Khaerul Rozi', 19, '089606972091', 'Jl. Nangka atas 3 jatibening baru bekasi', 'Elektro', 1, 2, 3, 14),
(232, 'muhamad rendy maulana', 6, '081294990797', 'jl.buntu kebayoran lama jakarta selatan', 'Informatika', 4, 2, 8, 6),
(233, 'Alif Haris Nugroho', 5, '081380169808', 'Jl.Raya Ciracas Gg.Rukem RT.04/RW.08 No.65 Ciracas Jakarta Timur', 'Informatika', 9, 3, 6, 2),
(234, 'Muhamad yusuf imadudien ', 1, '085783997692', 'Perum pondok damai blok l1 no 10,cileungsi, bogor', 'Informatika', 3, 7, 6, 4),
(235, 'Aldi fadillah', 19, '087881305753', 'jl.jati padang 3', 'Mesin', 2, 7, 10, 1),
(236, 'Nikko Esa Perdana', 2, '081319104649', 'Bekasi', 'Informatika', 2, 2, 6, 10),
(237, 'Tenny cornelia andaresta', 20, '081519764148', 'Cimpaeun depok', 'Informatika', 7, 1, 7, 5),
(238, 'Mario dwi prakoso', 8, '089519766134', 'Jln tridharma utama 4 rt05 no 12a rw 012', 'Informatika', 4, 2, 11, 3),
(239, 'PANDU DWI LAKSONO ', 19, '085819683584', 'Jl.lembang baru 3a no 49 Ciledug Tangerang', 'Informatika', 7, 1, 9, 3),
(240, 'Rizki Gunawan', 9, '085210328579', 'Jl regalia ciracas', 'Informatika', 4, 4, 9, 3),
(241, 'Fathi muhammad', 16, '6287796085158', 'Kpn.malaka rt04 RW06', 'Informatika', 6, 3, 6, 5),
(242, 'Reynaldi Ramadhani Pratama', 19, '081384420549', 'Cluster Acropolis De Wijaya Blok AB.18 Sukahati Cibinong, Kab Bogor.', 'Mesin', 2, 3, 6, 9),
(243, 'Mohammad Rinaldi', 19, '081297159759', 'Jln lap tembak gg mesjid rt02 rw06', 'Informatika', 1, 4, 10, 5),
(244, 'Argo Andhika Nugroho', 14, '0813101469891', 'Jln,kaumpandak no:13, RT/RW :03/10', 'Informatika', 7, 2, 7, 3),
(245, 'Wisnu Putra Praditya', 9, '089525455245', 'Palsigunung Rt.04/04 No.41 Tugu,Cimanggis - Depok', 'Informatika', 2, 4, 6, 8),
(246, 'KevinSigitDwiNugroho', 8, '083841593133', 'Jl.malaka Gg.bambu petung', 'Mesin', 3, 4, 7, 6),
(247, 'Raykhan fathan mubina', 16, '083896813636', 'Kp kresek pasir al amin kecamatan kresek provinsi banten kabupaten tangerang', 'Informatika', 3, 5, 7, 5),
(248, 'Yuwani eka Widyastuti ', 9, '087875965838', 'Jl.kelapa dua babakan rt03/04no26 kec setu Tangerang selatan', 'Informatika', 4, 3, 7, 6),
(249, 'IRFAN ANNAZIB', 4, '089503142953', 'Depok, ko Sindang karsa Tapos depok', 'Elektro', 3, 6, 5, 6),
(250, 'Mahfudz Ahnan Al Faruq', 8, '089528933619', 'mahfudzahnan@gmail.com', 'Informatika', 7, 5, 6, 2),
(251, 'Khiyaam.dimas.w', 4, '0895396922346', 'Jl.portiara', 'Elektro', 5, 3, 7, 5),
(252, 'Fajry octodira septiano', 9, '087775213691', 'Jl. Tegal parang Utara IV', 'Informatika', 10, 2, 3, 5),
(254, 'Ahmad fauzi ', 16, '082114901940 ', 'Jl pondok jaya', 'Informatika', 6, 1, 8, 5),
(255, 'Arya Bima Prastowo ', 11, '085725374627 ', 'Kav Sawah Indah 1 Rt02/05 Kelurahan Marga Mulya Kecamatan Bekasi Utara', 'Elektro', 10, 5, 3, 2),
(256, 'Muhammad Alvin Rama', 16, '081949752330', 'Jl. Halimah rt03/rw03 no.6b ulujami,pesanggrahan jakarta selatan', 'Informatika', 5, 1, 11, 3),
(257, 'ROJO AGUNG RIZQI', 4, '0895412934199', 'Jl.mahakam Rt:15 rw:06 no: 46  kel. Rambutan kec.ciracas', 'Mesin', 6, 5, 7, 2),
(258, 'Muhamad Adi Yaksa', 1, '089633236874', 'Jl. Mustika ratu , Ciracas RT004/RW004', 'Informatika', 6, 5, 6, 3),
(260, 'IWAN MAHYUDIN', 2, '081296908939', 'Jl.bambu kuning utara, rt.011/rw.02, kelurahan bambu apus, kecamatan cipayung', 'Informatika', 2, 3, 2, 3),
(261, 'Dede Suhendar ', 19, '083906597761', 'Ujung Harapan Gang Al-ikhlas2b No. 14 Rt. 002/043', 'Elektro', 3, 2, 6, 9),
(262, 'Khairunnisa Fajriyanti', 19, '0895351359359', 'Jl. H. Baping Gang Usaha, Ciracas Jakarta Timur', 'Informatika', 8, 1, 5, 6),
(263, 'Damar Wisnu Wijaya', 19, '081290949768', 'JL. MELATI RT12/09 KELAPA DUA WETAN CIRACAS JAKARTA TIMUR', 'Informatika', 3, 2, 9, 6),
(264, 'Irfan Zaky Yurisna', 14, '081287065705', 'Perum. Telaga Harapan Blok C22/11 RT07, RW 12', 'Informatika', 6, 2, 9, 3),
(265, 'Ahmad Jafar Sadiq', 1, '085817488461', 'Perumahan Griya Asri 2, blok k4 no.39, tambun selatan, desa sumber jaya', 'Informatika', 3, 6, 4, 7),
(266, 'Juan Mohammad Ishlah', 19, '085775175725', 'Jl. Guru mughni. Gg. Kembang. No 8', 'Informatika', 8, 3, 7, 2),
(267, 'SUKMA GIRI PRATAMA', 11, '081911715088', 'Jalan Pondok Labu 1 bawah no.12', 'Informatika', 5, 3, 7, 5),
(268, 'TRYADI PURWO HANDOKO', 18, '0895342564602', 'Jl.Anggrek Merah 1 No. 5 RT. 006/RW.025 Kel. Kaliabang Tengah Kec.Bekasi Utara Kota Bekasi', 'Elektro', 8, 1, 8, 3),
(269, 'Dzakwaan Hari Abdurrahmaan', 8, '085959477043', 'Pondok Tirta Mandala Tahap 5 blok G4 no 1 rt 02 rw 26', 'Elektro', 2, 4, 10, 4),
(270, 'salman', 15, '087719722848', 'kp.rawa sapi rt 03/09', 'Mesin', 3, 3, 11, 3),
(271, 'Taufan Septiansyah', 16, '089640578424', 'Perm Griya Persada blok H no 11 karang asem barat Citeureup Bogor', 'Informatika', 6, 3, 7, 4),
(272, 'izar hairul anam', 11, '082112381683', 'Kp.cilangkap RT04/04 kel. cilangkap kec.tapos kota depok', 'Informatika', 6, 2, 6, 6),
(273, 'Maruf Dicky Prayogo', 8, '089651902071', 'Perumahan griya asri taman mini Blok G2 No 20 RT 004 RW 023', 'Informatika', 2, 2, 8, 8),
(274, 'Bani hamdani', 15, '0895352830793', 'Desa. sitiwinangun blok. kebagusan kec.jamblang kab. Cirebon', 'Mesin', 5, 4, 4, 7),
(275, 'Ryan Hendro Cahyo', 16, '089662789309', 'Jl.saedah RT 010/007 Kel. Tengah Kec. Kramat Jati Jakarta Timur 13540', 'Mesin', 4, 5, 5, 6),
(276, 'Fikri Muwaffaq', 16, '089505538529', 'Jl. H. Amsir Cipinang Melayu,  Jakarta Timur', 'Mesin', 6, 5, 5, 4),
(277, 'Adi wiratmoko', 19, '0895397824515', 'Cilangkap', 'Mesin', 3, 5, 8, 4),
(279, 'Diaz Azmiraldy', 14, '087882999805', 'Jl.muhammad.alif.01.kukusan.beji.depok', 'Mesin', 1, 3, 8, 8),
(280, 'Ahmad Dzaki', 2, '089654449715', 'Jl.Albashor Rt.004 / Rw.003 Kel.Dukuh Kec.Kramat Jati Jakarta Timur', 'Informatika', 3, 4, 9, 4),
(281, 'Surya Hikmat Rahadian', 4, '089512260663', 'Desa cibalongsari RT06/01 kec Klari kab Karawang', 'Informatika', 3, 3, 4, 10),
(283, 'Muhammad Sulthony Aulia', 19, '087777476748', 'Jl, Cipinang Cempedak IV RT.05 RW.06 No.14 Kel. Cipinang Cempedak Kec. Jatinegara JAKARTA TIMUR', 'Informatika', 5, 3, 11, 1),
(284, 'Havila Arga Zulkarnain ', 11, '089665758176', 'Gang cemara parhoed no.148', 'Mesin', 2, 6, 5, 7),
(285, 'Mochammad Raihan', 10, '081585460562', 'curug induk no 83 rt3 rw3 kel curug kec bogor barat 16113', 'Informatika', 7, 1, 5, 7),
(286, 'Dika indra putra', 1, '081297494260', 'jl.kapuk jakarta barat', 'Informatika', 7, 3, 4, 6),
(287, 'Andreyansyah al fitri', 4, '085890013998', 'Jl kh muhasyim 2 011/06 jakarta selatan', 'Informatika', 4, 2, 12, 2),
(288, 'RASYID MAYLANDHANI PRASETYAWAN', 1, '0895603088729', 'Depok Baru', 'Elektro', 5, 3, 11, 1),
(289, 'Bagas dharma putra', 1, '082312184500', 'Cileungsi,bogor timur', 'Mesin', 4, 3, 9, 4),
(290, 'Muhammad Naufal Arfiansyah', 6, '082213058593', 'Taman Rahayu Regency B4 No 63 RT 02 RW 08 Kel. Ciketing Udik Kec. Bantar Gebang Kab. Bekasi', 'Informatika', 0, 2, 16, 2),
(291, 'wisnu muflhi werdhana', 9, '089679853931 ', 'jln. lapan gg.lewa XIV rt.14/08 kecamatan pekayon kelurahan pasar rebo', 'Mesin', 3, 6, 7, 4),
(292, 'Anwar Hidayat', 16, '081517313897', 'JL. Abdullah IV No 12 Rt 10 Rw 06 Kecamatan Taman Sari Kelurahan Krukut Jakarta Barat', 'Informatika', 7, 2, 8, 3),
(293, 'M. Naufal Fadrurrahman', 6, '08119740999', 'Jakarta Utara Asrama Yon Ang Air', 'Mesin', 2, 3, 6, 9),
(294, 'Rahmad setyajie', 19, '089618054495', 'Gang baru jl.anggrek merah II no 28 rt.06/025', 'Elektro', 4, 0, 10, 6),
(295, 'Farhan budianto', 14, '085692616870', 'Perum bukit cikasungka tahap 8 blok BFF 19 no 1 RT 06 RW 12', 'Informatika', 3, 1, 9, 7),
(296, 'Gilang bonie wiryawan', 11, '081287030361', 'Kp. Siluman rt 05/05 no.66 ds.Mangun Jaya Kec.Tambun Selatan Kab.Bekasi 17510', 'Elektro', 1, 8, 3, 8),
(297, 'Nazilul Muttaqin Nautica', 16, '082233113140', 'Jl.swadaya kebon no.09', 'Mesin', 7, 3, 8, 2),
(299, 'Muhamad Zidan Farhan', 10, '089607719831', 'Jln Rorotan 2 RT 05/04 Jakarta Utara', 'Mesin', 3, 9, 3, 5),
(300, 'Rivaldo Gabrielleo', 11, '085314103147', 'Jalan baru kampung rambutan', 'Informatika', 5, 3, 6, 6),
(301, 'Rismawati', 8, '085254314398', 'Jalan tanah merdeka kp rambutan Rusunawah', 'Informatika', 2, 4, 5, 9),
(302, 'Muhammad Aditya Pratama', 11, '0895360927172', 'Jl. Lembah Nirmala 1 blok A No.6 Rt 14/Rw14 Mekarsari, Cimanggis, Depok, Jawa Barat', 'Informatika', 6, 2, 7, 5),
(304, 'Fauzi Setiawan Akbar', 1, '085711233993', 'Kp.Ciherang jln.kramat III,Tapos-Depok', 'Informatika', 3, 5, 4, 8),
(305, 'andre setiawan', 14, '081240580348', 'jalan petamburan II', 'Informatika', 4, 4, 7, 5),
(306, 'Abdul Aziz Farhan Hidayat', 1, '085814948598', 'CitraIndahCity, Bukit Angsana Blok AU59/05 KECAMATAN:JONGGOL KABUPATEN:BOGOR', 'Informatika', 3, 2, 9, 6),
(307, 'Supriatman', 14, '085881492964', 'Jl. Asgo 2', 'Informatika', 6, 6, 4, 4),
(308, 'Reza Purnama Saputra', 11, '089530930488', 'Jl.masjid al munir', 'Informatika', 7, 4, 7, 2),
(309, 'Rikky Jaya Subita', 1, '082110716667', 'jl.sallamun IV no.75 rt007/003 jakarta timur', 'Informatika', 6, 3, 6, 5),
(310, 'MUHAMAD AZKA BAINUR RIZKI', 1, '081908029353', 'DESA GUNUNG AGUNG KEC  BUMIJAWA KAB TEGAL', 'Mesin', 4, 9, 3, 4),
(311, 'Arya Haydari Ramadhan', 10, '089652449216', 'Jalan kayu tinggi rt 003 rw 006 no 179, Cakung Timur, Cakung, Jakarta Timur', 'Informatika', 6, 1, 11, 2),
(312, 'Fikri muhammad krisdi', 19, '083894380228', 'Jl.lumbu timur no.143', 'Informatika', 2, 1, 8, 9),
(313, 'Aldin saktia fahrizal ', 10, '089503204391', 'Jln raya muchtar sawangan Depok Gg gandaria Rt 01/011', 'Informatika', 4, 2, 5, 9),
(314, 'Dimas Adi Prananda', 3, '089509566395', 'Jl.Belly GG mekar 7 no 16', 'Informatika', 10, 1, 7, 2),
(315, 'Dwi Setiawan', 2, '089604578303', 'Bekasi', 'Elektro', 4, 11, 2, 5),
(316, 'Aditya Pratama', 16, '087780407012', 'Lebak indah,serang,banten', 'Informatika', 7, 4, 4, 5),
(317, 'Dwi setiono', 3, '0878779o8928', 'Jl.bojongrangkong rt.006/rw.08', 'Informatika', 2, 6, 6, 6),
(318, 'Muhammad Bahtiar ', 10, '085885500206', 'jl masjid no 31 rt 02/02 harjamukti cimanggis depok', 'Elektro', 5, 11, 4, 0),
(319, 'Muhammad Dimas Prasetyo', 10, '081218678755', 'Jl.perintis rt/rw 011/01 No39 cipayung jakarta timur', 'Informatika', 7, 5, 6, 2),
(320, 'Wulan Nur Anjani Atikah ', 8, '089639433848', 'Jl. gunung jati 6 d28 nomor 1 rt 03/19 perum. kopassus pelita 2 sukatani, tapos, depok', 'Informatika', 6, 2, 4, 10),
(321, 'Siti namira nurulita', 10, '085714527811 (wa)', 'Bogor', 'Informatika', 10, 2, 5, 3),
(322, 'Dwi Putra Pamungkas', 10, '08978321005', 'Perum. Taman wanasari indah blok k3/5', 'Mesin', 5, 2, 8, 5),
(323, 'Agus komar', 1, '082231521054', 'Bogor timur, cileungsi', 'Elektro', 4, 2, 10, 4),
(324, 'Bonar Fauzi lubis', 4, '083873643196', 'Jl.masjid At-Taqwa gang bumur 3 nomor 86 kelurahan jatirangga kecamatan Jatisampurna kota Bekasi jawa barat', 'Mesin', 9, 7, 1, 3),
(325, 'Muhammad Yusuf Rendi', 1, '089503303930', 'Jln Bojong pondok terong RT 03 Rw 013 no 104', 'Informatika', 6, 6, 3, 5),
(326, 'ADITYA FIRMANSYAH', 3, '082312388614', 'Jl. Kebon Nanas Selatan III Rt.009/005', 'Informatika', 7, 5, 4, 4),
(327, 'Muhamad alvian syah', 10, '0896412927980', 'Perumahan citra indah bukit pinus q19/51', 'Elektro', 2, 3, 8, 7),
(328, 'Fathurrahman assidiq', 10, '085780169831', 'Kp.buaran rt 01/01 ds.lambangsari,kec-Tambun selatan', 'Mesin', 5, 5, 4, 6),
(329, 'Dimas Subhan', 10, '08998180816', 'Sidamukti, Sukamaju, Cilodong, Depok', 'Informatika', 5, 5, 5, 5),
(330, 'Abdullah Faqih ', 14, '085810382828 ', 'Jln. Jelambar Utama IV Gg Abadi RT 008 RW 08 NO. 27', 'Informatika', 5, 0, 11, 4),
(331, 'andrew sergio de rusli', 14, '081212685185', 'perm. pamulang elok blok c2 no 2', 'Informatika', 5, 7, 6, 2),
(332, 'Azzam Abdusalam', 3, '081223288131', 'Cipete utara', 'Elektro', 8, 7, 4, 1),
(333, 'Resal Alfarabi', 14, '0895620151997', 'Jl.trubus 1 dalam ,pondok cabe ilir', 'Mesin', 3, 2, 10, 5),
(334, 'Muhammad Raffie Rifzany', 10, '087782100035', 'Jln. Villa Indah III blok A no.7 Kebayoran Lama Jakarta Selatan', 'Informatika', 13, 1, 6, 0),
(335, 'Fahri Erdiansyah', 8, '083142572714', 'Bekasi', 'Elektro', 3, 0, 8, 9),
(336, 'Dimas Nur Cholish Ibrahim', 10, '081297449346', 'Jl. H. Degel Amin, Rt03/01 No. 100, Kp. Bendungan, Kel. Cilodong, Kec. Cilodong, Depok', 'Mesin', 4, 8, 4, 4),
(337, 'Achmad Zakaria', 3, '08871662741', 'Jl.bacang 006/07 no.1 C2 Rawasari Cempaka putih Jakarta pusat', 'Mesin', 6, 1, 11, 2),
(338, 'Tegar Imaniar kusnanto', 3, '085860347954', 'Jl.swadaya v dlm 2 RT.005 RW.024 Kaliabang tengah,Bekasi Utara bekasi', 'Mesin', 4, 4, 8, 4),
(339, 'ARIF FIKRIYANSAH', 11, '081280617272', 'Perumahan villa mutiara blok L23 no 21 Cibitung, Bekasi', 'Mesin', 2, 1, 10, 7),
(340, 'Ilham Nur Hidayat', 1, '0895323059691', 'Kp batas indah, kel pondok betung, kec pondok aren, tangerang selatan', 'Informatika', 4, 5, 5, 6),
(341, 'Devy kusrini', 3, '089510729008', 'Kp.beting jaya No 20 RT/RW 006/018 kel.Tugu utara kec.koja Jakarta utara', 'Informatika', 4, 4, 4, 8),
(342, 'Andika saputra', 1, '089667746520', 'jln aceng cinangka sawangan depok jawa barat', 'Informatika', 5, 3, 6, 6),
(343, 'Alfandi Safira', 1, '081219769496', 'Kota Bekasi, Pondok Melati, Jatirahayu, Kp. Bulak Poncol RT 007/018 No. 24', 'Informatika', 10, 1, 7, 2),
(344, 'Jascha adjie mulia', 11, '082298131786', 'Jl.Ahmad no52 rt15/03 kp makasar jaktim', 'Informatika', 7, 3, 5, 5),
(345, 'Ali Abdul Rizki', 10, '0895333538244', 'Jl.alternatif cibubur desa nagrak rt04/03 kec.gunung putri kab.bogor', 'Informatika', 6, 3, 9, 2),
(346, 'Adam nur alfajri', 3, '6281213067051', 'Kebagusan 1 gang mangga 2 kecamatan pasar minggu', 'Informatika', 3, 3, 10, 4),
(347, 'Muhammad Ardhi Andreansyah', 10, '083819202922', 'Kp. Rawahingkik Rt01/08 cileungsi, Bogor', 'Informatika', 2, 6, 8, 4),
(348, 'Rizki Baniumili', 9, '08988087362', 'Jl. Kesatrian VI Jakarta Timur', 'Elektro', 1, 4, 4, 11),
(349, 'Muchammat Arifin Afifulhakh', 1, '089693659888', 'Jl.H. Baping rt 07/06. kel.susukan, kec. ciracas jakarta timur', 'Mesin', 6, 6, 5, 3),
(350, 'izaz Rizka Syahbiela ', 3, '082176764259', 'Jl. Tugu karya II cipondoh', 'Informatika', 7, 1, 7, 5),
(351, 'Aditya Pradana Purbayani', 6, '082210419414', 'Pd kacang maharta A9/10 pondok aren tangerang selatan', 'Mesin', 2, 5, 7, 6),
(352, 'achmadrasidwidodo', 1, '089636355251', 'jln suci GG regalia susukan ciracas', 'Mesin', 6, 6, 2, 6),
(353, 'Afrisal khoirulloh', 7, '087771658986', 'Jln tipar cakung', 'Mesin', 5, 3, 4, 8),
(354, 'Gerit Gernandi', 1, '08983962287', 'Jl. Serut Rt. 004/004 No. 84 Pondok Ranggon Jakarta Timur', 'Mesin', 5, 2, 9, 4),
(355, 'Abimanyu Hermawan', 14, '085772352169', 'Jl. H Mustofa no.23E', 'Informatika', 3, 1, 14, 2),
(356, 'Muhammad Adi Samsul', 10, '089659971632', 'Jl.bona  gg, swadaya 1 Rt 004/Rw 003', 'Informatika', 3, 4, 6, 7),
(357, 'Muhammad Faiq Ramadhan', 1, ' 6289663169352', 'Jln. Hj. Siun kelurahan ceger', 'Informatika', 8, 2, 9, 1),
(358, 'Muhammadsyah Putra Bani', 8, '087786481928', 'Jl.Gandaria No.38A rt.03/06, kel. Ratujaya, kec. Cipayung, Kota Depok', 'Mesin', 4, 3, 6, 7),
(359, 'Rilla danyputri', 7, '083147519090', 'Jl jengki,gg melati RT 11 RW 04 No 5 kel.kebon pala,kec.makassar,halim Jakarta timur', 'Informatika', 4, 3, 5, 8),
(360, 'Yoga Apriananda', 11, '089629631816', 'Kp.Jati Baru RT 13 RW 06 Ds.Tanjungsari Kec.Cikarang Utara Kab. Bekasi', 'Mesin', 8, 1, 7, 4),
(361, 'Muhammad Farhan', 14, '087887165381', 'Perum bukit Cikasungka, blok BFF 19 no 9, Tahap 8B , Kec: Solear, Kab: Tangerang, Prov : Banten', 'Informatika', 1, 4, 5, 10),
(362, 'Dicky septia rizki', 3, '085714457163', 'Jl Raya hankam', 'Mesin', 8, 3, 6, 3),
(364, 'Yeltsin Sumarwan', 1, '085893445161', 'Tangerang selatan, bintaro sektor 3a', 'Mesin', 4, 4, 4, 8),
(365, 'Dede ismail', 1, '089611702258', 'Desa kertaungaran rt.005/003 nomor 24, kecamatan sindangagung kabupaten Kuningan', 'Informatika', 7, 1, 8, 4),
(367, 'Muhammad Usamah Rabbani', 7, '081219113200', 'Jalan Intisari Raya,No.2A, Rt03 Rw09', 'Informatika', 7, 1, 7, 5),
(368, 'Adistia Anggita Nugraheni', 1, '­081932209363', 'Cipinang muara', 'Informatika', 8, 6, 1, 5),
(369, 'Afri Handri setiawan', 1, '08159257473', 'Jl masjid raya no.7 larangan selatan tangeran', 'Elektro', 5, 5, 5, 5),
(370, 'Fajrul falah juniaro', 1, '085877258592', 'Bantarkawung,pengarasan', 'Elektro', 4, 4, 6, 6),
(371, 'Bunayya Ahmad Al Fatah', 7, '088234018570', 'Jalan Bangka 2c. No.6 Kecamatan Mampang Prapatan, kelurahan Pela Mampang, Jakarta Selatan', 'Informatika', 7, 1, 11, 1),
(372, 'Fakhri Muhammad Isham', 7, '081314140972', 'GG.H.NOIN RT.004/RW.01 KEL.BARU KEC.PASAR REBO', 'Informatika', 5, 4, 1, 10),
(373, 'Muhammad Azmi', 7, '089618414080', 'Jl.Batu Ampar', 'Elektro', 7, 4, 5, 4),
(374, 'TRIYANTI', 7, '083101891601', 'Kampung gedong jalan dana karya Rt10/08 nomor 52', 'Informatika', 5, 5, 6, 4),
(375, 'jose andres', 14, '083875104753', 'kp.pahlawan, kec.panimbang, kab.pandeglang', 'Elektro', 4, 4, 2, 10),
(376, 'MUCHAMMAD ARFIAN AZ-ZUHDI', 11, '081291534816', 'Jl raya Bogor km 33, gang sawo rt07/02 no.66, kelurahan Curug, kecamatan Cimanggis, kota Depok', 'Informatika', 4, 3, 5, 8),
(377, 'Taufik Hidayat', 7, '082213704955', 'Jl.teratai 2d blok d8 no.18 rt.001 rw024, cibiting bekasi', 'Informatika', 3, 6, 8, 3),
(378, 'Rafif Sidqi mokobombang', 7, '08559895967', 'Sulawesi Utara', 'Informatika', 6, 3, 3, 8),
(379, 'M,Fadhil Darfiansyah', 7, '082378971118', 'kompleks kopasus cijantung', 'Informatika', 2, 5, 5, 8),
(381, 'Gunawan Setiawan', 7, '081513748358', 'jln.h.iming no 38a rt 04 rw 02 beji depok', 'Mesin', 1, 3, 6, 10),
(382, 'Zulfikar Pangestu', 7, '0895369460715', 'Komplek TNI-AL Ciangsana Blok E12/10', 'Elektro', 4, 5, 3, 8),
(383, 'Muhammad Kamal', 1, '081294159625', 'Griya Kenari Mas Blok C2 No1', 'Informatika', 11, 0, 8, 1),
(384, 'Rizky Triswardana', 7, '087883763122', 'jl. belly v no. 22 Pekayon, pasar rebo, jakarta timur', 'Elektro', 0, 1, 12, 7),
(386, 'Ilkhanul khalik', 7, '082189831428', 'Ragunan', 'Informatika', 8, 3, 6, 3),
(387, 'Diky Radha Hernawan', 7, '08161112048', 'H baping , jalan subur Rt01/06 No 42', 'Informatika', 5, 5, 6, 4),
(388, 'Muhammad Zaki Farhan', 7, '0897-0164-991', 'Jl. Pancoran Barat VIII F RT 10 RW 03', 'Informatika', 7, 2, 7, 4),
(389, 'Ryan Ramadhan', 7, '087875066741', 'Jl. Rancho indah rt.11 rw 02 no.15 tanjung barat', 'Informatika', 3, 1, 14, 2),
(390, 'Muhamad Adil Hakim', 7, '0895615262293', 'Pinang tangerang', 'Informatika', 5, 8, 4, 3),
(391, 'Tio Chandra Widiansyah', 7, '08568994950', 'Jln.Tipar cakung. KP.Baru Rt/Rw:002/008 kel.Cakung barat kec.Cakung Jakarta Timur', 'Mesin', 5, 5, 3, 7),
(392, 'Fadil Fadlullah', 1, '081321274465', 'Kp. Gintung Desa. Limbangan RT 05/03 Kec. Sukaraja Kab. Sukabumi', 'Informatika', 2, 2, 9, 7),
(394, 'Cecep hadi permana', 3, '081398414395', 'Jalan kemang kelurahan sukatani kecamatan tapos kota depok', 'Informatika', 5, 3, 7, 5),
(395, 'Angga Novka Alana ', 1, '081328143078', 'ds kimak kec.merawang kab. bangka prov.kepulauan bangka belitung', 'Informatika', 5, 6, 5, 4),
(399, 'Muhammad Syah Ramadan', 7, '0895331742314', 'jalan pekapuran GG.H.Basyar Rt.05/022 No.42 Tapos-Depok', 'Mesin', 5, 5, 4, 6),
(400, 'Ghufron Akbar', 6, '089629908159', 'Jl.Karya Barat II No.11 Rt08/03 Kel.Wijaya Kusuma Kec.Grogol Petamburan Jakarta Barat', 'Mesin', 2, 5, 2, 11),
(401, 'Faldi wibowo', 5, '089653117025', 'Kp.sindangkarsa rt.001/005 sukamaju baru kec.tapos kota depok', 'Mesin', 4, 4, 8, 3),
(402, 'Muhammad alfariq Fatahillah ', 7, '085894099727', 'Jelambar , Grogol Petamburan', 'Mesin', 3, 5, 3, 9),
(403, 'Angga Dwipayana', 7, '087889477604', 'Jl.asmin,kecamatan ciracas, kelurahan susukan, Rt/Rt:0903', 'Informatika', 3, 3, 7, 7),
(404, 'Muhamad Firman Mawahda ', 12, '087884026867', 'Kelapa Dua Wetan III, jl. praji rt006/01 no.120 kel. kelapa dua wetan kec. ciracas jakarta timur', 'Informatika', 4, 1, 14, 1),
(405, 'rismawati', 15, '085718457429', 'jl. merica NO.58', 'Informatika', 8, 3, 5, 4),
(409, 'Muhammad farhan al rifqi', 13, '083898432441', 'Hj.nisin no.25', 'Mesin', 3, 1, 12, 4),
(410, 'Asep maihendra', 15, '087784452644', 'Kp.Kebayunan rt03/20 no.2 Tapos Depok', 'Elektro', 9, 1, 7, 3),
(411, 'Razman rifany', 15, '0895328458679', 'Jl.raya bogor rt005/015 no.1 kampung kramat', 'Informatika', 2, 6, 10, 2),
(412, 'Muhamad Feardy Casnur', 16, '089669646671', 'Kp. kojan Jl warung gantung Rt05/06 Kec.  kalideres kel. kalideres Jakarta Barat', 'Informatika', 3, 2, 11, 4),
(413, 'Faridh Qalaniy', 16, '081288821686', 'Jl. Kenanga Raya no.17 RT.05/005, Depok Jaya, Pancoran Mas, Depok', 'Elektro', 3, 4, 11, 2),
(414, 'Dhimas Anwar Putra', 16, '081290605498', 'kp jatijajar 2,rt 06/09,Tapos ,Depok', 'Informatika', 2, 2, 8, 8),
(417, 'Langgeng Arif Pambudi', 15, '0895321731165', 'ko sidamukti RT 07 RW 02 nomor 63 Cilodong depok', 'Elektro', 1, 5, 11, 3),
(418, 'Hary Ardika', 15, '082299009430', 'kp. kawidaran RT20 RW04, Desa Cibadak, Kecamatan Cikupa, Kabupaten Tangerang', 'Informatika', 8, 4, 8, 0),
(419, 'Rudi mardianto', 3, '082268519466', 'Jl haji ahmat r rt7 rw4', 'Elektro', 5, 4, 5, 6),
(420, 'Hisyam Fahdurohman Edmar', 14, '089606181010', 'Gg h juki', 'Informatika', 2, 4, 8, 6),
(421, 'Muhamad Kahfi', 1, '087883669340', 'Jl.Tebet Barat X C/2', 'Informatika', 7, 3, 4, 6),
(422, 'Panggih bagas nur afitro', 3, '081226843092', 'Purbalingga,jawa tengah', 'Informatika', 2, 4, 7, 7),
(424, 'Imam Arifin', 5, '089529329835', 'Tlajung Cikeas Udik Gunung Putri Bogor', 'Informatika', 5, 1, 12, 2),
(426, 'Widhi gunawan', 18, '0895333541781', 'Griya kenari mas blok f1 no 17', 'Informatika', 1, 7, 6, 6),
(427, 'FAJAR SIDIK', 18, '081212062303', 'Kp cibuntu', 'Informatika', 5, 3, 3, 9),
(428, 'aditya ervansyah', 14, '085364680023', 'padang', 'Elektro', 5, 9, 4, 2),
(429, 'Taufan Septiyansyah', 16, '089640578424', 'Perm Griya Persada blok H no.11 karang asem barat Citeureup Bogor', 'Informatika', 6, 3, 7, 4),
(430, 'M. Aura Septa Firdaus', 18, '083819466285', 'Kp. Cibeureum rt 04/rw 05 kec. Cileungsi', 'Informatika', 5, 4, 9, 2),
(431, 'Danu sakralludi', 11, '085693606400', 'Sawangan, Depok', 'Elektro', 3, 4, 10, 3),
(432, 'Yoga Aji Pratama', 14, '087786400967', 'Jl asmin rt 06 rw 03 susukan ciracas jakarta timur', 'Informatika', 5, 5, 6, 4),
(433, 'Farid Ridwan', 8, '082127358243', 'Jl.munir syahbana Kp.jati baru ds.setia darma kec.tambun selatan kab.Bekasi', 'Mesin', 4, 2, 11, 3),
(434, 'Ilham Aji Pangestu', 18, '0895372446352', 'Jl. Avia blok a1 no 24', 'Informatika', 7, 3, 3, 7),
(435, 'Abdullah azam', 15, '081574523548', 'jln. cendrawasih 9 rt 10/10 blok c3 no 142 Sawangan Depok', 'Informatika', 5, 5, 8, 2),
(436, 'Muhammad Fadil Wicaksana', 8, '0895331020476', 'Jl villa pertiwi Km 37 Rt 03 / Rw 16 blok N4 No 22 Kel sukamaju Kec cilodong', 'Informatika', 5, 1, 5, 9),
(437, 'Fariz Alghofiqi', 1, '089638307568', 'Jl raya karanggan no 33 RT 02 RW 10 kelurahan Puspasari kecamatan Citeureup kabupaten Bogor', 'Elektro', 2, 1, 9, 8),
(438, 'Zulkarnain Ferdiansyah', 14, '089681301181', 'Jl.kemang ko.babakan RT01/RW10 Sukatani, Tapos, Depok.', 'Mesin', 6, 5, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mabadetail`
--

CREATE TABLE `tbl_mabadetail` (
  `id` int(11) NOT NULL,
  `id_maba` int(11) NOT NULL,
  `jenis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mabadetail`
--

INSERT INTO `tbl_mabadetail` (`id`, `id_maba`, `jenis`) VALUES
(154, 78, 'Melankolis'),
(155, 78, 'Koleris'),
(156, 79, 'Sanguinis'),
(157, 80, 'Plegmatis'),
(158, 80, 'Koleris'),
(159, 81, 'Plegmatis'),
(160, 82, 'Melankolis'),
(161, 83, 'Sanguinis'),
(162, 84, 'Koleris'),
(163, 85, 'Koleris'),
(164, 86, 'Plegmatis'),
(165, 86, 'Koleris'),
(166, 87, 'Melankolis'),
(168, 89, 'Plegmatis'),
(169, 90, 'Melankolis'),
(170, 91, 'Plegmatis'),
(171, 92, 'Melankolis'),
(172, 93, 'Plegmatis'),
(173, 94, 'Plegmatis'),
(174, 95, 'Plegmatis'),
(175, 96, 'Plegmatis'),
(176, 97, 'Koleris'),
(177, 98, 'Sanguinis'),
(178, 99, 'Plegmatis'),
(179, 100, 'Plegmatis'),
(180, 101, 'Koleris'),
(181, 102, 'Melankolis'),
(182, 103, 'Plegmatis'),
(183, 104, 'Plegmatis'),
(184, 105, 'Koleris'),
(185, 106, 'Melankolis'),
(186, 107, 'Plegmatis'),
(187, 107, 'Koleris'),
(188, 108, 'Koleris'),
(189, 109, 'Plegmatis'),
(190, 110, 'Koleris'),
(191, 111, 'Melankolis'),
(192, 112, 'Sanguinis'),
(193, 112, 'Plegmatis'),
(194, 113, 'Sanguinis'),
(195, 114, 'Melankolis'),
(196, 115, 'Melankolis'),
(197, 115, 'Plegmatis'),
(198, 116, 'Plegmatis'),
(199, 117, 'Sanguinis'),
(200, 118, 'Plegmatis'),
(201, 119, 'Sanguinis'),
(202, 119, 'Melankolis'),
(203, 120, 'Plegmatis'),
(204, 121, 'Sanguinis'),
(205, 121, 'Koleris'),
(206, 122, 'Plegmatis'),
(207, 123, 'Melankolis'),
(208, 124, 'Plegmatis'),
(209, 125, 'Melankolis'),
(210, 126, 'Plegmatis'),
(211, 127, 'Melankolis'),
(212, 128, 'Plegmatis'),
(213, 129, 'Plegmatis'),
(214, 129, 'Koleris'),
(215, 130, 'Melankolis'),
(216, 130, 'Koleris'),
(217, 131, 'Sanguinis'),
(218, 132, 'Melankolis'),
(219, 133, 'Plegmatis'),
(220, 134, 'Melankolis'),
(221, 135, 'Plegmatis'),
(222, 136, 'Melankolis'),
(223, 137, 'Koleris'),
(224, 138, 'Melankolis'),
(225, 138, 'Koleris'),
(226, 139, 'Melankolis'),
(227, 140, 'Melankolis'),
(228, 140, 'Plegmatis'),
(229, 141, 'Plegmatis'),
(230, 142, 'Sanguinis'),
(231, 143, 'Sanguinis'),
(232, 144, 'Melankolis'),
(233, 145, 'Plegmatis'),
(234, 146, 'Koleris'),
(235, 147, 'Sanguinis'),
(236, 148, 'Melankolis'),
(237, 149, 'Melankolis'),
(238, 149, 'Koleris'),
(239, 150, 'Sanguinis'),
(240, 151, 'Melankolis'),
(241, 152, 'Koleris'),
(242, 153, 'Plegmatis'),
(243, 155, 'Melankolis'),
(244, 156, 'Melankolis'),
(245, 157, 'Sanguinis'),
(246, 157, 'Melankolis'),
(247, 157, 'Plegmatis'),
(248, 157, 'Koleris'),
(249, 158, 'Sanguinis'),
(250, 159, 'Melankolis'),
(251, 160, 'Plegmatis'),
(252, 161, 'Sanguinis'),
(253, 162, 'Melankolis'),
(254, 163, 'Melankolis'),
(255, 164, 'Melankolis'),
(256, 165, 'Melankolis'),
(257, 166, 'Sanguinis'),
(258, 167, 'Sanguinis'),
(259, 168, 'Sanguinis'),
(260, 169, 'Sanguinis'),
(261, 170, 'Plegmatis'),
(262, 171, 'Plegmatis'),
(263, 172, 'Sanguinis'),
(264, 172, 'Koleris'),
(265, 173, 'Sanguinis'),
(266, 174, 'Sanguinis'),
(267, 174, 'Plegmatis'),
(268, 175, 'Melankolis'),
(269, 176, 'Melankolis'),
(270, 177, 'Melankolis'),
(271, 177, 'Plegmatis'),
(272, 178, 'Plegmatis'),
(273, 179, 'Sanguinis'),
(274, 180, 'Melankolis'),
(275, 180, 'Plegmatis'),
(276, 181, 'Plegmatis'),
(277, 182, 'Plegmatis'),
(278, 183, 'Plegmatis'),
(279, 184, 'Koleris'),
(280, 185, 'Koleris'),
(281, 186, 'Melankolis'),
(282, 187, 'Plegmatis'),
(283, 188, 'Plegmatis'),
(284, 189, 'Melankolis'),
(285, 189, 'Plegmatis'),
(286, 190, 'Sanguinis'),
(287, 191, 'Sanguinis'),
(288, 192, 'Melankolis'),
(289, 193, 'Melankolis'),
(290, 194, 'Sanguinis'),
(291, 195, 'Sanguinis'),
(292, 195, 'Melankolis'),
(293, 196, 'Plegmatis'),
(294, 197, 'Sanguinis'),
(295, 198, 'Melankolis'),
(296, 199, 'Plegmatis'),
(297, 200, 'Sanguinis'),
(298, 201, 'Plegmatis'),
(299, 202, 'Melankolis'),
(300, 203, 'Sanguinis'),
(301, 203, 'Plegmatis'),
(302, 204, 'Sanguinis'),
(303, 205, 'Sanguinis'),
(304, 206, 'Melankolis'),
(305, 207, 'Sanguinis'),
(306, 208, 'Sanguinis'),
(307, 209, 'Melankolis'),
(308, 210, 'Melankolis'),
(309, 210, 'Plegmatis'),
(310, 211, 'Sanguinis'),
(311, 212, 'Sanguinis'),
(312, 213, 'Plegmatis'),
(313, 214, 'Plegmatis'),
(314, 215, 'Melankolis'),
(315, 216, 'Plegmatis'),
(316, 217, 'Plegmatis'),
(317, 218, 'Koleris'),
(318, 219, 'Plegmatis'),
(319, 220, 'Plegmatis'),
(320, 221, 'Plegmatis'),
(321, 222, 'Plegmatis'),
(322, 223, 'Koleris'),
(323, 224, 'Sanguinis'),
(324, 225, 'Plegmatis'),
(325, 226, 'Plegmatis'),
(326, 227, 'Sanguinis'),
(327, 228, 'Melankolis'),
(328, 228, 'Plegmatis'),
(329, 229, 'Plegmatis'),
(330, 230, 'Plegmatis'),
(331, 232, 'Plegmatis'),
(332, 231, 'Sanguinis'),
(333, 233, 'Melankolis'),
(334, 234, 'Koleris'),
(335, 235, 'Plegmatis'),
(336, 238, 'Plegmatis'),
(337, 237, 'Melankolis'),
(338, 236, 'Sanguinis'),
(339, 237, 'Plegmatis'),
(340, 239, 'Plegmatis'),
(341, 240, 'Plegmatis'),
(342, 241, 'Melankolis'),
(343, 241, 'Plegmatis'),
(344, 242, 'Sanguinis'),
(345, 243, 'Plegmatis'),
(346, 244, 'Melankolis'),
(347, 244, 'Plegmatis'),
(348, 245, 'Sanguinis'),
(349, 246, 'Plegmatis'),
(350, 247, 'Plegmatis'),
(351, 248, 'Plegmatis'),
(352, 249, 'Sanguinis'),
(353, 249, 'Koleris'),
(354, 250, 'Melankolis'),
(355, 251, 'Plegmatis'),
(356, 252, 'Melankolis'),
(361, 254, 'Plegmatis'),
(362, 255, 'Melankolis'),
(363, 256, 'Plegmatis'),
(364, 257, 'Plegmatis'),
(365, 258, 'Melankolis'),
(366, 258, 'Plegmatis'),
(371, 260, 'Sanguinis'),
(372, 260, 'Koleris'),
(373, 261, 'Sanguinis'),
(374, 262, 'Melankolis'),
(375, 263, 'Plegmatis'),
(376, 264, 'Plegmatis'),
(377, 265, 'Sanguinis'),
(378, 266, 'Melankolis'),
(379, 267, 'Plegmatis'),
(380, 268, 'Melankolis'),
(381, 268, 'Plegmatis'),
(382, 269, 'Plegmatis'),
(383, 270, 'Plegmatis'),
(384, 271, 'Plegmatis'),
(385, 272, 'Sanguinis'),
(386, 272, 'Melankolis'),
(387, 272, 'Plegmatis'),
(388, 273, 'Sanguinis'),
(389, 273, 'Plegmatis'),
(390, 274, 'Sanguinis'),
(391, 275, 'Sanguinis'),
(392, 276, 'Melankolis'),
(393, 277, 'Plegmatis'),
(395, 279, 'Sanguinis'),
(396, 279, 'Plegmatis'),
(397, 280, 'Plegmatis'),
(398, 281, 'Sanguinis'),
(400, 283, 'Plegmatis'),
(401, 284, 'Sanguinis'),
(402, 285, 'Sanguinis'),
(403, 285, 'Melankolis'),
(404, 286, 'Melankolis'),
(405, 287, 'Plegmatis'),
(406, 288, 'Plegmatis'),
(407, 289, 'Plegmatis'),
(408, 290, 'Plegmatis'),
(409, 291, 'Plegmatis'),
(410, 292, 'Plegmatis'),
(411, 293, 'Sanguinis'),
(412, 294, 'Plegmatis'),
(413, 295, 'Plegmatis'),
(414, 296, 'Sanguinis'),
(415, 296, 'Koleris'),
(416, 297, 'Plegmatis'),
(418, 299, 'Koleris'),
(419, 300, 'Sanguinis'),
(420, 300, 'Plegmatis'),
(421, 301, 'Sanguinis'),
(422, 302, 'Plegmatis'),
(423, 304, 'Sanguinis'),
(424, 305, 'Plegmatis'),
(425, 306, 'Plegmatis'),
(426, 307, 'Melankolis'),
(427, 307, 'Koleris'),
(428, 308, 'Melankolis'),
(429, 308, 'Plegmatis'),
(430, 309, 'Melankolis'),
(431, 309, 'Plegmatis'),
(432, 310, 'Koleris'),
(433, 311, 'Plegmatis'),
(434, 312, 'Sanguinis'),
(435, 313, 'Sanguinis'),
(436, 314, 'Melankolis'),
(437, 315, 'Koleris'),
(438, 316, 'Melankolis'),
(439, 317, 'Sanguinis'),
(440, 317, 'Plegmatis'),
(441, 317, 'Koleris'),
(442, 318, 'Koleris'),
(443, 319, 'Melankolis'),
(444, 320, 'Sanguinis'),
(445, 321, 'Melankolis'),
(446, 322, 'Plegmatis'),
(447, 323, 'Plegmatis'),
(448, 324, 'Melankolis'),
(449, 325, 'Melankolis'),
(450, 326, 'Melankolis'),
(451, 325, 'Koleris'),
(452, 327, 'Plegmatis'),
(453, 328, 'Sanguinis'),
(454, 329, 'Sanguinis'),
(455, 329, 'Melankolis'),
(456, 329, 'Plegmatis'),
(457, 329, 'Koleris'),
(458, 330, 'Plegmatis'),
(459, 331, 'Koleris'),
(460, 332, 'Melankolis'),
(461, 333, 'Plegmatis'),
(462, 334, 'Melankolis'),
(463, 335, 'Sanguinis'),
(464, 336, 'Koleris'),
(465, 337, 'Plegmatis'),
(466, 338, 'Plegmatis'),
(467, 339, 'Plegmatis'),
(468, 340, 'Sanguinis'),
(469, 341, 'Sanguinis'),
(470, 342, 'Sanguinis'),
(471, 342, 'Plegmatis'),
(472, 343, 'Melankolis'),
(473, 344, 'Melankolis'),
(474, 345, 'Plegmatis'),
(475, 346, 'Plegmatis'),
(476, 347, 'Plegmatis'),
(477, 348, 'Sanguinis'),
(478, 349, 'Melankolis'),
(479, 349, 'Koleris'),
(480, 350, 'Melankolis'),
(481, 350, 'Plegmatis'),
(482, 351, 'Plegmatis'),
(483, 352, 'Sanguinis'),
(484, 352, 'Melankolis'),
(485, 352, 'Koleris'),
(486, 353, 'Sanguinis'),
(487, 354, 'Plegmatis'),
(488, 355, 'Plegmatis'),
(489, 356, 'Sanguinis'),
(490, 357, 'Plegmatis'),
(491, 358, 'Sanguinis'),
(492, 359, 'Sanguinis'),
(493, 360, 'Melankolis'),
(494, 361, 'Sanguinis'),
(495, 362, 'Melankolis'),
(497, 364, 'Sanguinis'),
(498, 365, 'Plegmatis'),
(503, 367, 'Melankolis'),
(504, 367, 'Plegmatis'),
(505, 368, 'Melankolis'),
(506, 369, 'Sanguinis'),
(507, 369, 'Melankolis'),
(508, 369, 'Plegmatis'),
(509, 369, 'Koleris'),
(510, 370, 'Sanguinis'),
(511, 370, 'Plegmatis'),
(512, 371, 'Plegmatis'),
(513, 372, 'Sanguinis'),
(514, 373, 'Melankolis'),
(515, 374, 'Plegmatis'),
(516, 375, 'Sanguinis'),
(517, 376, 'Sanguinis'),
(518, 377, 'Plegmatis'),
(519, 378, 'Sanguinis'),
(520, 379, 'Sanguinis'),
(522, 381, 'Sanguinis'),
(523, 382, 'Sanguinis'),
(524, 383, 'Melankolis'),
(525, 384, 'Plegmatis'),
(530, 386, 'Melankolis'),
(531, 387, 'Plegmatis'),
(532, 388, 'Melankolis'),
(533, 388, 'Plegmatis'),
(534, 389, 'Plegmatis'),
(535, 390, 'Koleris'),
(536, 391, 'Sanguinis'),
(537, 392, 'Plegmatis'),
(539, 394, 'Plegmatis'),
(540, 395, 'Koleris'),
(550, 399, 'Sanguinis'),
(551, 400, 'Sanguinis'),
(552, 401, 'Plegmatis'),
(553, 402, 'Sanguinis'),
(554, 403, 'Sanguinis'),
(555, 403, 'Plegmatis'),
(556, 404, 'Plegmatis'),
(557, 405, 'Melankolis'),
(561, 409, 'Plegmatis'),
(562, 410, 'Melankolis'),
(563, 411, 'Plegmatis'),
(564, 412, 'Plegmatis'),
(565, 413, 'Plegmatis'),
(566, 414, 'Sanguinis'),
(567, 414, 'Plegmatis'),
(570, 417, 'Plegmatis'),
(571, 418, 'Melankolis'),
(572, 418, 'Plegmatis'),
(573, 419, 'Sanguinis'),
(574, 420, 'Plegmatis'),
(575, 421, 'Melankolis'),
(576, 422, 'Sanguinis'),
(577, 422, 'Plegmatis'),
(579, 424, 'Plegmatis'),
(581, 426, 'Koleris'),
(582, 427, 'Sanguinis'),
(583, 429, 'Plegmatis'),
(584, 430, 'Plegmatis'),
(585, 431, 'Plegmatis'),
(586, 432, 'Plegmatis'),
(587, 433, 'Plegmatis'),
(588, 434, 'Sanguinis'),
(589, 434, 'Melankolis'),
(590, 435, 'Plegmatis'),
(591, 436, 'Sanguinis'),
(592, 437, 'Plegmatis'),
(593, 438, 'Melankolis');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soal`
--

CREATE TABLE `tbl_soal` (
  `id` int(11) NOT NULL,
  `no_soal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soal`
--

INSERT INTO `tbl_soal` (`id`, `no_soal`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soaldetail`
--

CREATE TABLE `tbl_soaldetail` (
  `id` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `pertanyaan` text NOT NULL,
  `tipe` enum('koleris','melankolis','sanguinis','plegmatis') NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soaldetail`
--

INSERT INTO `tbl_soaldetail` (`id`, `id_soal`, `pertanyaan`, `tipe`, `status`) VALUES
(1, 1, 'a)	Melakukan sesuatu sampai selesai, sebelum mulai melakukan hal yang lain.', 'melankolis', 'a1'),
(2, 1, 'b)	Selalu ceria dan punya selera humor yang baik.', 'sanguinis', 'a2'),
(3, 1, 'c)	Bisa meyakinkan orang lain dengan logika dan fakta, bukannya menggunakan pesona atau kekuasaan.', 'koleris', 'a3'),
(4, 1, 'd)	Tampil tenang dan berusaha menghindar dari keadaan yg kacau / berkonflik.', 'plegmatis', 'a4'),
(9, 2, 'a)	Suka membantu atau membuat orang lain merasa senang.', 'sanguinis', 'a5'),
(10, 2, 'b)	Memperlakukan orang lain dengan rasa segan dan hormat.', 'melankolis', 'a6'),
(11, 2, 'c)	Menahan diri dalam menunjukkan emosi atau antusiasme.', 'plegmatis', 'a7'),
(12, 2, 'd)	Bisa bertindak cepat dan efektif, hampir dalam semua situasi. ', 'koleris', 'a8'),
(13, 3, 'a)	Orang yang mudah menerima keadaan atau situasi apa saja.', 'plegmatis', 'a9'),
(14, 3, 'b)	Orang mandiri yang bisa sepenuhnya mengandalkan penilaian dan kemampuannya sendiri.', 'koleris', 'a10'),
(15, 3, 'c)	Senang mengamati orang lain dan situasi yang sedang terjadi.', 'melankolis', 'a11'),
(16, 3, 'd)	Penuh gairah dalam hidup.', 'sanguinis', 'a12'),
(17, 4, 'a)	Orang yang mengatur segalanya secara teratur dan sistematik.', 'melankolis', 'a13'),
(18, 4, 'b)	Bisa menerima apa saja. Orang yang cepat menyesuaikan diri ketika situasi sudah berubah.', 'plegmatis', 'a14'),
(19, 4, 'c)	Bicara terang-terangan tanpa perlu menahan diri.', 'koleris', 'a15'),
(20, 4, 'd)	Orang yang periang dan bisa meyakinkan orang lain bahwa segala-galanya akan beres.', 'sanguinis', 'a16'),
(21, 5, 'a)	Orang yang jarang memulai percakapan, lebih suka sebagai pendengar dan bukan mendahului bercerita.', 'plegmatis', 'a17'),
(22, 5, 'b)	Sangat bisa diandalkan, teguh, setia dan penuh pengabdian. ', 'melankolis', 'a18'),
(23, 5, 'c)	Punya rasa humor yang tinggi dan bisa membuat cerita apa saja menjadi menarik.', 'sanguinis', 'a19'),
(24, 5, 'd)	Bisa menguasai orang lain dan menyebabkan orang lain menurut', 'koleris', 'a20'),
(25, 6, 'a)	Berani mengambil resiko tak kenal takut.', 'koleris', 'a21'),
(26, 6, 'b)	Orang yang menyenangkan sebagai teman.', 'sanguinis', 'a22'),
(27, 6, 'c)	Orang yang sabar dan perasa.', 'plegmatis', 'a23'),
(28, 6, 'd)	Melakukan segala sesuatu secara berurutan, dengan ingatan yang kuat tentang segala hal yang terjadi.', 'melankolis', 'a24'),
(29, 7, 'a)	Si pemikir yang penuh pertimbangan.', 'melankolis', 'a25'),
(30, 7, 'b)	Teguh pada tujuan, tidak mudah dibelokkan.', 'koleris', 'a26'),
(31, 7, 'c)	Orang yang banyak berbicara, suka menceritakan kisah lucu dan membuat orang lain tertawa', 'sanguinis', 'a27'),
(32, 7, 'd)	Mudah menerima pemikiran dan ide-ide orang lain.', 'plegmatis', 'a28'),
(33, 8, 'a)	Pendengar yang baik.', 'plegmatis', 'a29'),
(34, 8, 'b)	Orang yang mengutamakan kesetiaan pada orang lain atau pada suatu pekerjaan.', 'melankolis', 'a30'),
(35, 8, 'c)	Pemimpin bagi orang di sekitarnya yakin pada cara yang dimilikinya.', 'koleris', 'a31'),
(36, 8, 'd)	Orang yang penuh semangat dalam kehidupan.', 'sanguinis', 'a32'),
(37, 9, 'a)	Mudah puas dengan apa yang dimiliki, jarang iri hati.', 'plegmatis', 'a33'),
(38, 9, 'b)	Memegang kepemimpinan dan mengharapkan orang lain mengikutinya.', 'koleris', 'a34'),
(39, 9, 'c)	Orang yang bergantung pada daftar tugas yang jelas dan terinci dalam bekerja.', 'melankolis', 'a35'),
(40, 9, 'd)	Orang yang menjadi pusat perhatian, menyenangkan dan dicintai orang lain.', 'sanguinis', 'a36'),
(41, 10, 'a)	Senang bekerja, merasa aneh bila menganggur.', 'koleris', 'a37'),
(42, 10, 'b)	Mudah bergaul, terbuka dan mudah diajak bicara.', 'plegmatis', 'a38'),
(43, 10, 'c)	Orang yang bisa & senang membuat suasana jadi meriah', 'sanguinis', 'a39'),
(44, 10, 'd)	Memasang standar prestasi yang tinggi pada diri sendiri dan orang lain.', 'melankolis', 'a40'),
(45, 11, 'a)	Enggan terlibat, terutama dalam hal-hal rumit', 'plegmatis', 'a41'),
(46, 11, 'b)	Sering memendam rasa tidak senang, akibat tersinggung pada sesuatu yg bisa jadi hanya merupakan kekhawatiran.', 'melankolis', 'a42'),
(47, 11, 'c)	Sulit menerima cara lain yang bukan caranya sendiri jika perlu hal tersebut dilawan.', 'koleris', 'a43'),
(48, 11, 'd)	Suka menceritakan kembali kejadian-kejadian yang dianggap bisa menghibur orang lain, \ntanpa menyadari bahwa mungkin saja cerita itu sdh diulang beberapa kali.', 'sanguinis', 'a44'),
(49, 12, 'a)	Tidak-sabaran bila melihat orang lain lamban dalam bekerja.', 'koleris', 'a45'),
(50, 12, 'b)	Mudah cemas dan khawatir.', 'melankolis', 'a46'),
(51, 12, 'c)	Merasa enggan membuat keputusan.', 'plegmatis', 'a47'),
(52, 12, 'd)	Lebih banyak bicara daripada mendengarkan, kadang-kadang menyela pembicaraan orang lain tanpa sadar.', 'sanguinis', 'a48'),
(53, 13, 'a)	Orang yang kadang-kadang dihindari orang lain karena ingin yang serba sempurna.', 'melankolis', 'a49'),
(54, 13, 'b)	Enggan terlibat dalam situasi kumpul-kumpul.', 'plegmatis', 'a50'),
(55, 13, 'c)	Orang yang sulit ditebak, bisa tiba-tiba senang, lalu mendadak jadi sedih, atau sudah berjanji sesuatu dan segera terlupa akan janjinya.', 'sanguinis', 'a51'),
(56, 13, 'd)	Sulit menunjukkan rasa sayang secara terbuka.', 'koleris', 'a52'),
(58, 14, 'a)	Mudah ngambek / patah-arang, tapi bisa segera melupakannya.', 'sanguinis', 'a53'),
(59, 14, 'b)	Senang berdebat.', 'koleris', 'a54'),
(60, 14, 'c)	Kurang senang dibebani target atau membuat target.', 'plegmatis', 'a55'),
(61, 14, 'd)	Mudah merasa tersingkirkan, walaupun orang lain sebenarnya tidak bermaksud demikian.', 'melankolis', 'a56'),
(62, 15, 'a)	Mudah merasa resah dan gelisah.', 'melankolis', 'a57'),
(63, 15, 'b)	Menarik diri dan lebih senang sendirian.', 'plegmatis', 'a58'),
(64, 15, 'c)	Senang menenggelamkan diri dalam pekerjaan.', 'koleris', 'a59'),
(65, 15, 'd)	Senang mendapat penghargaan dan dipuji orang lain.', 'sanguinis', 'a60'),
(66, 16, 'a)	Lamban dalam bertindak atau berpikir.', 'plegmatis', 'a61'),
(67, 16, 'b)	Teguh dalam pendirian. Orang lain sering menyebut sebagai “keras kepala”.', 'koleris', 'a62'),
(68, 16, 'c)	Senang menjadi pusat perhatian.', 'sanguinis', 'a63'),
(69, 16, 'd)	Tidak mudah percaya, selalu mempertanyakan apa yang ada dibalik ucapan orang lain.', 'melankolis', 'a64'),
(70, 17, 'a)	Penyendiri dan lebih senang menghindar dari orang lain.', 'melankolis', 'a65'),
(71, 17, 'b)	Tidak ragu dalam menyatakan bahwa sayalah yang benar, sehingga lebih baik jika saya yang memimpin.', 'koleris', 'a66'),
(72, 17, 'c)	Mengukur pekerjaan atau kegiatan dengan berapa banyak tenaga yang harus dikeluarkan.', 'plegmatis', 'a67'),
(73, 17, 'd)	Orang yang terdengar suara / tawanya dari jauh.', 'sanguinis', 'a68'),
(74, 18, 'a)	Lambat untuk memulai sesuatu, sangat perlu dorongan orang lain.', 'plegmatis', 'a69'),
(75, 18, 'b)	Sulit fokus pada satu hal mudah berubah perhatiannya.', 'sanguinis', 'a70'),
(76, 18, 'c)	Mudah marah bila orang lain tidak bertindak cukup cepat atau mengerjakan apa yg diperintahkan kepada mereka.', 'koleris', 'a71'),
(77, 18, 'd)	Tidak mudah mempercayai orang lain begitu saja, bila belum dipelajari dengan seksama.', 'melankolis', 'a72'),
(78, 19, 'a)	Suka menyimpan dendam (secara sadar / tidak sadar).', 'melankolis', 'a73'),
(79, 19, 'b)	Menyukai kegiatan baru terus-menerus, bosan terhadap hal yang monoton.', 'sanguinis', 'a74'),
(80, 19, 'c)	Tidak bersedia atau berusaha menghindar dari keharusan untuk terlibat.', 'plegmatis', 'a75'),
(81, 19, 'd)	Cepat mengambil tindakan, kadang tanpa pertimbangan matang.', 'koleris', 'a76'),
(82, 20, 'a)	Cerdik, cenderung menghalalkan segala cara demi mencapai tujuan yang diyakini benar.', 'koleris', 'a77'),
(83, 20, 'b)	Selalu berpikir kritis, melihat segala sesuatu dari sisi negatifnya dulu.', 'melankolis', 'a78'),
(84, 20, 'c)	Mudah berkompromi bahkan ketika sebenarnya dalam posisi yang benar, bisa mengalah demi menghindari konflik.', 'plegmatis', 'a79'),
(85, 20, 'd)	Mudah beralih perhatiannya.', 'sanguinis', 'a80');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `level`) VALUES
(1, 'monica', '123456', 1),
(2, 'panitia1', '123456', 1),
(3, 'panitia2', '123456', 1),
(4, 'panitia3', '123456', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_hasil`
--
ALTER TABLE `tbl_hasil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_maba`
--
ALTER TABLE `tbl_maba`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_mabadetail`
--
ALTER TABLE `tbl_mabadetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_maba` (`id_maba`);

--
-- Indexes for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `no_soal` (`no_soal`);

--
-- Indexes for table `tbl_soaldetail`
--
ALTER TABLE `tbl_soaldetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_soal` (`id_soal`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_hasil`
--
ALTER TABLE `tbl_hasil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_maba`
--
ALTER TABLE `tbl_maba`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=439;

--
-- AUTO_INCREMENT for table `tbl_mabadetail`
--
ALTER TABLE `tbl_mabadetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=594;

--
-- AUTO_INCREMENT for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_soaldetail`
--
ALTER TABLE `tbl_soaldetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_mabadetail`
--
ALTER TABLE `tbl_mabadetail`
  ADD CONSTRAINT `tbl_mabadetail_ibfk_1` FOREIGN KEY (`id_maba`) REFERENCES `tbl_maba` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_soaldetail`
--
ALTER TABLE `tbl_soaldetail`
  ADD CONSTRAINT `tbl_soaldetail_ibfk_1` FOREIGN KEY (`id_soal`) REFERENCES `tbl_soal` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
