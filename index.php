<?php

session_start();
unset($_SESSION['ngecek']);
session_destroy();
// if(!isset($_SESSION['ngecek'])){
// 	header('location:index.php');
// }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>TES KEPRIBADIAN</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="assets/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/vendor/animate/animate.css">
<!--===============================================================================================-->	
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="assets/css/util.css">
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-50 p-b-90">
				<form class="login100-form validate-form flex-sb flex-w" action=page1.php method="get">
					<span class="login100-form-title p-b-51">
						TES KEPRIBADAN
					</span>	
					<div class="wrap-input100 validate-input m-b-16 field" data-validate = "Harus diisi">
						<input class="input100" type="text" name="nama" placeholder="Nama">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-16 field" data-validate = "Harus diisi">
						<div class="wrap-input100 validate-input m-b-16" data-validate = "Harus diisi">
							<select name="prodi" class="input100">
							    <option value="Informatika">Informatika</option>
							    <option value="Mesin">Mesin</option>
							    <option value="Elektro">Elektro</option>
							  </select>
							<span class="focus-input100"></span>
						</div>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-16 field" data-validate = "Harus diisi">
						<div class="wrap-input100 validate-input m-b-16" data-validate = "Harus diisi">
							<select name="gugus" class="input100">
							    <option value=1>1</option>
							    <option value=2>2</option>
							    <option value=3>3</option>
							    <option value=4>4</option>
							    <option value=5>5</option>
							    <option value=6>6</option>
							    <option value=7>7</option>
							    <option value=8>8</option>
							    <option value=9>9</option>
							    <option value=10>10</option>
							    <option value=11>11</option>
							    <option value=12>12</option>
							    <option value=13>13</option>
							    <option value=14>14</option>
							    <option value=15>15</option>
							    <option value=16>16</option>
							    <option value=17>17</option>
							    <option value=18>18</option>
							    <option value=19>19</option>
							    <option value=20>20</option>

							  </select>
							<span class="focus-input100"></span>
						</div>
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Harus diisi">
						<input class="input100" type="text" name="telp" placeholder="No Hp">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 validate-input m-b-16" data-validate = "Harus diisi">
						<input class="input100" type="text" name="alamat" placeholder="Alamat">
						<span class="focus-input100"></span>
					</div>
					
					
					<div class="container-login100-form-btn m-t-17 actions">
						<button class="login100-form-btn" type="submit">
							MASUK
						</button>
					</div>

				</form>
						
							<br>
							<center>
							Created by - MRMR &copy; 2018 
							</center>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/bootstrap/js/popper.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/daterangepicker/moment.min.js"></script>
	<script src="assets/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="assets/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="assets/js/main.js"></script>

</body>
</html>