-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 18, 2018 at 09:14 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `petaftuhamka_kepribadian`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hasil`
--

CREATE TABLE `tbl_hasil` (
  `id` int(11) NOT NULL,
  `jenis` enum('Sanguinis','Melankolis','Plegmatis','Koleris') NOT NULL,
  `ciri` text NOT NULL,
  `kekuatan` text NOT NULL,
  `kelemahan` text NOT NULL,
  `cocok` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_hasil`
--

INSERT INTO `tbl_hasil` (`id`, `jenis`, `ciri`, `kekuatan`, `kelemahan`, `cocok`) VALUES
(1, 'Sanguinis', 'Cenderung populer dan disenangi banyak orang, Hidupnya penuh dengan bunga warna-warni, Senang berbicara tanpa dihentikan, Gejolak emosinya bergelombang dan transparan, Senang menolong orang lain tetapi tidak dapat jadi sandaran, Pada suatu saat dia teriak kegirangan kemudian beberapa saat dia bisa jadi menangis tersedu-sedu, Sedikit pelupa, sulit berkonsentrasi, cenderung berfikir pendek, dan hidupnya serba tidak teratur.', 'Senang berbicara, Secara fisik memegang pendengar, demonstrative, dan emosional; Ceria, Penuh rasa ingin tahu, Mudah berubah (banyaknya kegiatan / keinginan), Berhati tulus dan kekanak-kanakan, Mudah berteman dan menyukai orang lain, Senang berkumpul (untuk bertemu dan berbicara), Mudah memaafkan dan tidak menyimpan dendam.', 'Membesar-besarkan suatu hal / kejadian, Susah untuk diam, Suara tertawa yang terlalu keras, Mudah ikut-ikutan atau dikendalikan oleh keadaan atau orang lain, Rentan konsentrasi pendek, Sering minta persetujuan termasuk hal-hal sepele, Mudah marah, Mudah berubah-ubah, Egois, Prioritas kegiatan kacau, Konsentrasi ke <b>How to spend money</b> daripada <b>How to earn / save money</b>', 'Artis, MC, Pengacara.'),
(2, 'Melankolis', 'Umumnya mereka ini suka dengan fakta-fakta, data-data, angka-angka dan sering sekali memikirkan segalanya secara mendalam . Dalam sebuah pertemuan, orang sanguinis selalu saja mendominasi pembicaraan, namun orang melankolis cenderung menganalisa, memikirkan, mempertimbangkan, lalu kalau bicara pastilah apa yang ia katakan betul-betul hasil yang ia pikirkan secara  mendalam sekali. <br> Orang melankolis selalu ingin serba sempurna, Segala sesuatu ingin teratur.', 'Analitis, mendalam, dan penuh pikiran; Serius dan bertujuan, serta berorientasi jadwal; Artistik, musikal dan kreatif (filsafat & puitis), Sensitif, Mau mengorbankan diri dan idealis, Standar tinggi dan perfeksionis, Hemat, Berteman dengan hati-hati, Sangat memperhatikan orang lain, Menghindari Perhatian, kreatif (sering terlalu kreatif), Melihat masalah dan mencari solusi pemecahan.', 'Cenderung melihat masalah dari sisi negatif (murung dan tertekan), Mengingat yang negatif & pendendam, Mudah merasa bersalah dan memiliki citra diri rendah, Lebih menekankan pada cara daripada tercapainya tujuan, Sulit bersosialisasi, Tertekan pada situasi yg tidak sempurna dan berubah-ubah, Melewatkan banyak waktu untuk menganalisa dan merencanakan, Sulit mengungkapkan perasaan (cenderung menahan kasih sayang), Rasa curiga yg besar (skeptis terhadap pujian), Standar yang terlalu tinggi sehingga sulit disenangi.', 'Seniman, Musisi, Penulis, dsb.'),
(3, 'Plegmatis', 'Ia mau merugi sedikit atau rela sakit, asalkan masalahnya nggak terus berkepanjangan; Kaum phlegmatis kurang bersemangat, kurang teratur dan serba dingin; Cenderung diam, kalem dan kalau memecahkan masalah umumnya sangat menyenangkan; Dengan sabar ia mau jadi pendengar yang baik, tapi kalau disuruh untuk mengambil keputusan ia akan terus menunda-nunda, Kadang sedikit serba salah berurusan dengan para plegmatis ini. Ibarat keledai, <b>kalau didorong ngambek, tapi kalau dibiarin nggak jalan</b>.', 'Mudah bergaul, santai, tenang dan teguh; Sabar, seimbang, dan pendengar yang baik; Tidak banyak bicara, tetapi cenderung bijaksana; Simpatik dan baik hati (sering menyembunyikan emosi), Kuat di bidang administrasi, dan cenderung ingin segalanya terorganisasi; Penengah masalah yg baik, Cenderung berusaha menemukan cara termudah, Baik dibawah tekanan, Menyenangkan dan tidak suka menyinggung perasaan, Rasa humor yg tajam, Senang melihat dan mengawasi, Berbelaskasihan dan peduli, Mudah diajak rukun dan damai.', 'Kurang antusias, terutama terhadap perubahan/ kegiatan baru; Takut dan khawatir, Menghindari konflik dan tanggung jawab; Keras kepala, sulit kompromi (karena merasa benar), Terlalu pemalu dan pendiam, Humor kering dan mengejek (Sarkatis), Kurang berorientasi pada tujuan, Sulit bergerak dan kurang memotivasi diri, Lebih suka sebagai penonton daripada terlibat, Tidak senang didesak-desak, Menunda-nunda / menggantungkan masalah.', 'Guru, Akuntan, dan lain sebagainya.'),
(4, 'Koleris', 'Suka sekali mengatur orang, Suka menunjuk atau memerintah orang, Senang dengan tantangan, suka petualangan; Orang-orang berusaha menghindar dan menjauh agar tak jadi korban karakternya yang suka mengatur dan tak mau kalah itu, Tegas, kuat, cepat dan tangkas dalam mengerjakan. Ia tak ingin ada penonton dalam aktivitasnya, bahkan tamu pun bisa saja ia suruh melakukan sesuatu untuknya. Akibat sifatnya yang <b>Bossy</b> itu membuat banyak orang koleris tak punya banyak teman.', 'Senang memimpin, membuat keputusan, dinamis dan aktif (Seorang Leader), Sangat memerlukan perubahan dan harus mengoreksi kesalahan, Berkemauan keras dan pasti untuk mencapai sasaran/ target, Bebas dan mandiri, Berani menghadapi tantangan dan masalah, Mencari pemecahan praktis dan bergerak cepat, Mendelegasikan pekerjaan dan orientasi berfokus pada produktivitas, Membuat dan menentukan tujuan, Terdorong oleh tantangan dan tantangan, Tidak begitu perlu teman, Biasanya benar dan punya visi ke depan.', 'Tidak sabar dan cepat marah, Senang memerintah, Terlalu bergairah dan susah untuk santai, Menyukai kontroversi dan pertengkaran, Terlalu kaku dan kuat/ keras, Tidak suka yang bertele-tele / terlalu rinci, Sering membuat keputusan tergesa-gesa ,Memanipulasi dan menuntut orang lain, cenderung memperalat orang lain;, Menghalalkan segala cara demi tercapainya tujuan, Mungkin selalu benar tetapi tidak popular, Tidak menyukai air mata dan emosi.', 'Biasanya berhubungan dengan kepemimpinan karena kaum ini mempunyai jiwa kepemimpinan yang sangat tinggi.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_maba`
--

CREATE TABLE `tbl_maba` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `gugus` int(11) NOT NULL,
  `no_hp` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `prodi` enum('Informatika','Elektro','Mesin','') NOT NULL,
  `mel` int(11) DEFAULT NULL,
  `kol` int(11) DEFAULT NULL,
  `pleg` int(11) DEFAULT NULL,
  `sang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_maba`
--

INSERT INTO `tbl_maba` (`id`, `nama`, `gugus`, `no_hp`, `alamat`, `prodi`, `mel`, `kol`, `pleg`, `sang`) VALUES
(53, 'Zack', 1, '081905241265', 'Jalan Letnan Arsyad No.15, RT 02/01, Kayuringin Jaya, Bekasi Selatan', 'Elektro', 9, 4, 3, 4),
(60, 'Monica Dwijayanti', 1, '081574999999', 'Jakarta', 'Informatika', 2, 5, 4, 9),
(64, 'Tony Stark', 3, '088888888', 'Stark Company', 'Informatika', 6, 2, 6, 6),
(65, 'Mermaid Man', 12, '089876531243', 'Bikini Bottom', 'Informatika', 2, 6, 6, 6),
(69, 'I Love You', 2, '084444444444', 'I need You', 'Mesin', 4, 6, 6, 4),
(70, 'Ian Andri', 1, '0818181', 'dEPOK', 'Mesin', 5, 6, 6, 3),
(71, 'adasd', 1, '2123', 'asdasd', 'Elektro', 2, 6, 4, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mabadetail`
--

CREATE TABLE `tbl_mabadetail` (
  `id` int(11) NOT NULL,
  `id_maba` int(11) NOT NULL,
  `jenis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mabadetail`
--

INSERT INTO `tbl_mabadetail` (`id`, `id_maba`, `jenis`) VALUES
(118, 53, 'Melankolis'),
(125, 60, 'Sanguinis'),
(130, 64, 'Sanguinis'),
(131, 64, 'Melankolis'),
(132, 64, 'Plegmatis'),
(133, 65, 'Sanguinis'),
(134, 65, 'Plegmatis'),
(135, 65, 'Koleris'),
(139, 69, 'Plegmatis'),
(140, 69, 'Koleris'),
(141, 70, 'Plegmatis'),
(142, 70, 'Koleris'),
(143, 71, 'Sanguinis');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soal`
--

CREATE TABLE `tbl_soal` (
  `id` int(11) NOT NULL,
  `no_soal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soal`
--

INSERT INTO `tbl_soal` (`id`, `no_soal`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soaldetail`
--

CREATE TABLE `tbl_soaldetail` (
  `id` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `pertanyaan` text NOT NULL,
  `tipe` enum('koleris','melankolis','sanguinis','plegmatis') NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soaldetail`
--

INSERT INTO `tbl_soaldetail` (`id`, `id_soal`, `pertanyaan`, `tipe`, `status`) VALUES
(1, 1, 'a)	Melakukan sesuatu sampai selesai, sebelum mulai melakukan hal yang lain.', 'melankolis', 'a1'),
(2, 1, 'b)	Selalu ceria dan punya selera humor yang baik.', 'sanguinis', 'a2'),
(3, 1, 'c)	Bisa meyakinkan orang lain dengan logika dan fakta, bukannya menggunakan pesona atau kekuasaan.', 'koleris', 'a3'),
(4, 1, 'd)	Tampil tenang dan berusaha menghindar dari keadaan yg kacau / berkonflik.', 'plegmatis', 'a4'),
(9, 2, 'a)	Suka membantu atau membuat orang lain merasa senang.', 'sanguinis', 'a5'),
(10, 2, 'b)	Memperlakukan orang lain dengan rasa segan dan hormat.', 'melankolis', 'a6'),
(11, 2, 'c)	Menahan diri dalam menunjukkan emosi atau antusiasme.', 'plegmatis', 'a7'),
(12, 2, 'd)	Bisa bertindak cepat dan efektif, hampir dalam semua situasi. ', 'koleris', 'a8'),
(13, 3, 'a)	Orang yang mudah menerima keadaan atau situasi apa saja.', 'plegmatis', 'a9'),
(14, 3, 'b)	Orang mandiri yang bisa sepenuhnya mengandalkan penilaian dan kemampuannya sendiri.', 'koleris', 'a10'),
(15, 3, 'c)	Senang mengamati orang lain dan situasi yang sedang terjadi.', 'melankolis', 'a11'),
(16, 3, 'd)	Penuh gairah dalam hidup.', 'sanguinis', 'a12'),
(17, 4, 'a)	Orang yang mengatur segalanya secara teratur dan sistematik.', 'melankolis', 'a13'),
(18, 4, 'b)	Bisa menerima apa saja. Orang yang cepat menyesuaikan diri ketika situasi sudah berubah.', 'plegmatis', 'a14'),
(19, 4, 'c)	Bicara terang-terangan tanpa perlu menahan diri.', 'koleris', 'a15'),
(20, 4, 'd)	Orang yang periang dan bisa meyakinkan orang lain bahwa segala-galanya akan beres.', 'sanguinis', 'a16'),
(21, 5, 'a)	Orang yang jarang memulai percakapan, lebih suka sebagai pendengar dan bukan mendahului bercerita.', 'plegmatis', 'a17'),
(22, 5, 'b)	Sangat bisa diandalkan, teguh, setia dan penuh pengabdian. ', 'melankolis', 'a18'),
(23, 5, 'c)	Punya rasa humor yang tinggi dan bisa membuat cerita apa saja menjadi menarik.', 'sanguinis', 'a19'),
(24, 5, 'd)	Bisa menguasai orang lain dan menyebabkan orang lain menurut', 'koleris', 'a20'),
(25, 6, 'a)	Berani mengambil resiko tak kenal takut.', 'koleris', 'a21'),
(26, 6, 'b)	Orang yang menyenangkan sebagai teman.', 'sanguinis', 'a22'),
(27, 6, 'c)	Orang yang sabar dan perasa.', 'plegmatis', 'a23'),
(28, 6, 'd)	Melakukan segala sesuatu secara berurutan, dengan ingatan yang kuat tentang segala hal yang terjadi.', 'melankolis', 'a24'),
(29, 7, 'a)	Si pemikir yang penuh pertimbangan.', 'melankolis', 'a25'),
(30, 7, 'b)	Teguh pada tujuan, tidak mudah dibelokkan.', 'koleris', 'a26'),
(31, 7, 'c)	Orang yang banyak berbicara, suka menceritakan kisah lucu dan membuat orang lain tertawa', 'sanguinis', 'a27'),
(32, 7, 'd)	Mudah menerima pemikiran dan ide-ide orang lain.', 'plegmatis', 'a28'),
(33, 8, 'a)	Pendengar yang baik.', 'plegmatis', 'a29'),
(34, 8, 'b)	Orang yang mengutamakan kesetiaan pada orang lain atau pada suatu pekerjaan.', 'melankolis', 'a30'),
(35, 8, 'c)	Pemimpin bagi orang di sekitarnya yakin pada cara yang dimilikinya.', 'koleris', 'a31'),
(36, 8, 'd)	Orang yang penuh semangat dalam kehidupan.', 'sanguinis', 'a32'),
(37, 9, 'a)	Mudah puas dengan apa yang dimiliki, jarang iri hati.', 'plegmatis', 'a33'),
(38, 9, 'b)	Memegang kepemimpinan dan mengharapkan orang lain mengikutinya.', 'koleris', 'a34'),
(39, 9, 'c)	Orang yang bergantung pada daftar tugas yang jelas dan terinci dalam bekerja.', 'melankolis', 'a35'),
(40, 9, 'd)	Orang yang menjadi pusat perhatian, menyenangkan dan dicintai orang lain.', 'sanguinis', 'a36'),
(41, 10, 'a)	Senang bekerja, merasa aneh bila menganggur.', 'koleris', 'a37'),
(42, 10, 'b)	Mudah bergaul, terbuka dan mudah diajak bicara.', 'plegmatis', 'a38'),
(43, 10, 'c)	Orang yang bisa & senang membuat suasana jadi meriah', 'sanguinis', 'a39'),
(44, 10, 'd)	Memasang standar prestasi yang tinggi pada diri sendiri dan orang lain.', 'melankolis', 'a40'),
(45, 11, 'a)	Enggan terlibat, terutama dalam hal-hal rumit', 'plegmatis', 'a41'),
(46, 11, 'b)	Sering memendam rasa tidak senang, akibat tersinggung pada sesuatu yg bisa jadi hanya merupakan kekhawatiran.', 'melankolis', 'a42'),
(47, 11, 'c)	Sulit menerima cara lain yang bukan caranya sendiri jika perlu hal tersebut dilawan.', 'koleris', 'a43'),
(48, 11, 'd)	Suka menceritakan kembali kejadian-kejadian yang dianggap bisa menghibur orang lain, \ntanpa menyadari bahwa mungkin saja cerita itu sdh diulang beberapa kali.', 'sanguinis', 'a44'),
(49, 12, 'a)	Tidak-sabaran bila melihat orang lain lamban dalam bekerja.', 'koleris', 'a45'),
(50, 12, 'b)	Mudah cemas dan khawatir.', 'melankolis', 'a46'),
(51, 12, 'c)	Merasa enggan membuat keputusan.', 'plegmatis', 'a47'),
(52, 12, 'd)	Lebih banyak bicara daripada mendengarkan, kadang-kadang menyela pembicaraan orang lain tanpa sadar.', 'sanguinis', 'a48'),
(53, 13, 'a)	Orang yang kadang-kadang dihindari orang lain karena ingin yang serba sempurna.', 'melankolis', 'a49'),
(54, 13, 'b)	Enggan terlibat dalam situasi kumpul-kumpul.', 'plegmatis', 'a50'),
(55, 13, 'c)	Orang yang sulit ditebak, bisa tiba-tiba senang, lalu mendadak jadi sedih, atau sudah berjanji sesuatu dan segera terlupa akan janjinya.', 'sanguinis', 'a51'),
(56, 13, 'd)	Sulit menunjukkan rasa sayang secara terbuka.', 'koleris', 'a52'),
(58, 14, 'a)	Mudah ngambek / patah-arang, tapi bisa segera melupakannya.', 'sanguinis', 'a53'),
(59, 14, 'b)	Senang berdebat.', 'koleris', 'a54'),
(60, 14, 'c)	Kurang senang dibebani target atau membuat target.', 'plegmatis', 'a55'),
(61, 14, 'd)	Mudah merasa tersingkirkan, walaupun orang lain sebenarnya tidak bermaksud demikian.', 'melankolis', 'a56'),
(62, 15, 'a)	Mudah merasa resah dan gelisah.', 'melankolis', 'a57'),
(63, 15, 'b)	Menarik diri dan lebih senang sendirian.', 'plegmatis', 'a58'),
(64, 15, 'c)	Senang menenggelamkan diri dalam pekerjaan.', 'koleris', 'a59'),
(65, 15, 'd)	Senang mendapat penghargaan dan dipuji orang lain.', 'sanguinis', 'a60'),
(66, 16, 'a)	Lamban dalam bertindak atau berpikir.', 'plegmatis', 'a61'),
(67, 16, 'b)	Teguh dalam pendirian. Orang lain sering menyebut sebagai “keras kepala”.', 'koleris', 'a62'),
(68, 16, 'c)	Senang menjadi pusat perhatian.', 'sanguinis', 'a63'),
(69, 16, 'd)	Tidak mudah percaya, selalu mempertanyakan apa yang ada dibalik ucapan orang lain.', 'melankolis', 'a64'),
(70, 17, 'a)	Penyendiri dan lebih senang menghindar dari orang lain.', 'melankolis', 'a65'),
(71, 17, 'b)	Tidak ragu dalam menyatakan bahwa sayalah yang benar, sehingga lebih baik jika saya yang memimpin.', 'koleris', 'a66'),
(72, 17, 'c)	Mengukur pekerjaan atau kegiatan dengan berapa banyak tenaga yang harus dikeluarkan.', 'plegmatis', 'a67'),
(73, 17, 'd)	Orang yang terdengar suara / tawanya dari jauh.', 'sanguinis', 'a68'),
(74, 18, 'a)	Lambat untuk memulai sesuatu, sangat perlu dorongan orang lain.', 'plegmatis', 'a69'),
(75, 18, 'b)	Sulit fokus pada satu hal mudah berubah perhatiannya.', 'sanguinis', 'a70'),
(76, 18, 'c)	Mudah marah bila orang lain tidak bertindak cukup cepat atau mengerjakan apa yg diperintahkan kepada mereka.', 'koleris', 'a71'),
(77, 18, 'd)	Tidak mudah mempercayai orang lain begitu saja, bila belum dipelajari dengan seksama.', 'melankolis', 'a72'),
(78, 19, 'a)	Suka menyimpan dendam (secara sadar / tidak sadar).', 'melankolis', 'a73'),
(79, 19, 'b)	Menyukai kegiatan baru terus-menerus, bosan terhadap hal yang monoton.', 'sanguinis', 'a74'),
(80, 19, 'c)	Tidak bersedia atau berusaha menghindar dari keharusan untuk terlibat.', 'plegmatis', 'a75'),
(81, 19, 'd)	Cepat mengambil tindakan, kadang tanpa pertimbangan matang.', 'koleris', 'a76'),
(82, 20, 'a)	Cerdik, cenderung menghalalkan segala cara demi mencapai tujuan yang diyakini benar.', 'koleris', 'a77'),
(83, 20, 'b)	Selalu berpikir kritis, melihat segala sesuatu dari sisi negatifnya dulu.', 'melankolis', 'a78'),
(84, 20, 'c)	Mudah berkompromi bahkan ketika sebenarnya dalam posisi yang benar, bisa mengalah demi menghindari konflik.', 'plegmatis', 'a79'),
(85, 20, 'd)	Mudah beralih perhatiannya.', 'sanguinis', 'a80');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `level`) VALUES
(1, 'monica', 'monica', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_hasil`
--
ALTER TABLE `tbl_hasil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_maba`
--
ALTER TABLE `tbl_maba`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_mabadetail`
--
ALTER TABLE `tbl_mabadetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_maba` (`id_maba`);

--
-- Indexes for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `no_soal` (`no_soal`);

--
-- Indexes for table `tbl_soaldetail`
--
ALTER TABLE `tbl_soaldetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_soal` (`id_soal`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_hasil`
--
ALTER TABLE `tbl_hasil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_maba`
--
ALTER TABLE `tbl_maba`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `tbl_mabadetail`
--
ALTER TABLE `tbl_mabadetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_soaldetail`
--
ALTER TABLE `tbl_soaldetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_mabadetail`
--
ALTER TABLE `tbl_mabadetail`
  ADD CONSTRAINT `tbl_mabadetail_ibfk_1` FOREIGN KEY (`id_maba`) REFERENCES `tbl_maba` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_soaldetail`
--
ALTER TABLE `tbl_soaldetail`
  ADD CONSTRAINT `tbl_soaldetail_ibfk_1` FOREIGN KEY (`id_soal`) REFERENCES `tbl_soal` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
